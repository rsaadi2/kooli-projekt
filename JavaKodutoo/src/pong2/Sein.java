package pong2;

import java.awt.Color;
import java.awt.Graphics;

public class Sein {

	public int x;
	public int y;

	int yVelocity = -3; //kiirus

	int laius = 40;
	int korgus = 80;

	public Sein(int alguskohtx, int alguskohty) {
		x = alguskohtx;
		y = alguskohty;
	}

	public void update() {

		y = y + yVelocity;

		{

			if (y < 50) {
				yVelocity = -yVelocity;
			} else if (y + 110 > Pong2.window_height) {
				yVelocity = -yVelocity;

			}

		}

	}

	public void paint(Graphics g) {
		g.setColor(Color.ORANGE);
		g.fillRect(x, y, laius, korgus); //värvime seina
	}

	public void reverseDirection() {
		yVelocity = -yVelocity; //meetod, mille kutsume välja Collusionis.
	}

	public void checkCollusionWith(Player player) {

		if (this.x + laius > player.getX() && this.x < player.getX() + player.width) {
			if (this.y + korgus > player.getY() && this.y < player.getY() + player.height) {
				reverseDirection();
				player.elud = player.elud - 1;

				if (player.kasOnVasakul == true) {
					player.x = 10;
					player.y = 200;
				}
				if (player.kasOnVasakul == false) {
					{
						player.x = 550;
						player.y = 200;
					}

				}
			}
		}
	}

}
