// Peamised tutorialid, mida kasutanud:
// https://www.youtube.com/watch?v=3jozT1LaMHY
// https://www.youtube.com/watch?v=j8qbeQprGEQ&list=PLkDNkRdp-C4lJmiUFq-SFlK3Ik18RPPM7



package pong2;

import javax.swing.JPanel;

import javax.swing.JFrame;

public class Pong2 extends JFrame {// see tähendab, et sellele klassile tulevad
									// kaasa.
	// Jframe omadused.
	static int window_width = 600;
	static int window_height = 380;

	public Pong2() {
		setSize(window_width, window_height); // JFrame meetodid
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		add(new GamePanel());
		setVisible(true);

	}

	public static void main(String[] args) {

		new Pong2();
	}

}
