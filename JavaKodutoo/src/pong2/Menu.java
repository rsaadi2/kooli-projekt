package pong2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Menu {
	public Rectangle Mangi = new Rectangle(85, 190, 110, 45);
	public Rectangle Abi = new Rectangle(215, 190, 110, 45);
	public Rectangle Lõpeta = new Rectangle(345, 190, 110, 45);

	public void paint(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;
		

		Font fnt0 = new Font("arial", Font.BOLD, 50);
		g.setFont(fnt0);
		g.setColor(Color.WHITE);
		g.drawString("Catch Me If You Can", 53, 100);

		Font fnt1 = new Font("arial", Font.BOLD, 22);
		g.setFont(fnt1);

		g.drawString("MÄNGI", Mangi.x + 17, Mangi.y + 30);
		g2d.draw(Mangi);
		g.drawString("ABI", Abi.x + 32, Abi.y + 30);
		g2d.draw(Abi);
		g.drawString("LÕPETA", Lõpeta.x + 11, Lõpeta.y + 30);
		g2d.draw(Lõpeta);

	}
}