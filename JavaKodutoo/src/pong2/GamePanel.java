package pong2;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class GamePanel extends JPanel implements ActionListener, KeyListener {

	Player player = new Player();

	Ball ball = new Ball(64, 111);
	Ball ball1 = new Ball(65, 51);
	Ball ball2 = new Ball(200, 125);
	Ball ball3 = new Ball(300, 333);
	Ball ball4 = new Ball(400, 456);

	Sein sein = new Sein(200, 59);
	Sein sein2 = new Sein(400, 200);
	Punkt punkt = new Punkt();

	public static enum STATE {
		MENU, GAME
	};

	public static STATE State = STATE.MENU;
	public Menu menu;

	public GamePanel() {

		Timer tim = new Timer(15, this);// Esimene argument utleb, kui
										// pik on paus. Teine argument -
		// mis ta siis teeb, kui paus tekib.
		tim.start();

		this.addKeyListener(this);// Jpanel meetod. This argumendina Łtlebki, et
									// see praegune klass on Listener!
		this.setFocusable(true); // kui pole aken aktiivne, siis klaviatuur
									// ka mitte.
		menu = new Menu();
		this.addMouseListener(new MouseInput());
	}

	void update() {
		if (State == STATE.GAME) {

			kasOnSurnud();//kutsume välja meetodi, et elud ja punktid resettida.

			player.update();

			ball.update();
			ball1.update();
			ball2.update();
			ball3.update();
			ball4.update();

			sein.update();
			sein2.update();

			punkt.update();

			
			ball.checkCollusionWith(player);
			ball1.checkCollusionWith(player);
			ball2.checkCollusionWith(player);
			ball3.checkCollusionWith(player);
			ball4.checkCollusionWith(player);
			sein.checkCollusionWith(player);
			sein2.checkCollusionWith(player);
			punkt.checkCollusionWith(player);

		
		} else if (State == STATE.MENU) {

		}
	}

	
	public void paintComponent(Graphics g) {

		// Siin kood, mis disainib mängu. Värvib akna ja kutsub klassides paint meetodid välja. 
		g.setColor(Color.BLACK); 
		g.fillRect(0, 0, Pong2.window_width, Pong2.window_height);
		if (State == STATE.GAME) {

			g.setColor(Color.BLUE);
			g.fillRect(0, 0, 50, Pong2.window_height);
			g.fillRect(Pong2.window_width - 50, 0, Pong2.window_height - 50, Pong2.window_height);
			g.setColor(Color.BLUE);
			g.fillRect(0, 0, Pong2.window_width, 50);
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, Pong2.window_width, 50 - 8);
			g.setColor(Color.BLUE);
			g.fillRect(0, 349, Pong2.window_width, 10);
			Font fnt0 = new Font("arial", Font.BOLD, 22);
			g.setFont(fnt0);
			g.setColor(Color.WHITE);
			g.drawString("Punktid: " + String.valueOf(player.punktid), 20, 30);
			Font fnt1 = new Font("arial", Font.BOLD, 22);
			g.setFont(fnt0);
			g.setColor(Color.WHITE);
			g.drawString("Elud: " + String.valueOf(player.elud), 495, 30);

			player.paint(g);

			ball.paint(g);
			ball1.paint(g);
			ball2.paint(g);
			ball3.paint(g);
			ball4.paint(g);
			sein.paint(g);
			sein2.paint(g);
			punkt.paint(g);
		} else if (State == STATE.MENU) {
			menu.paint(g);
		}

	}

	public void actionPerformed(ActionEvent e) { // see on meetod, mida
													// actionlistener vajab.
		// See on meie gameloop.
		update(); // kutsub üleval oleva update meetodi, mis omakorda kutsub klasside update meetodid. 
		repaint(); // Jpanel meetod, kutsub iga kord pšrast update paint
					// componendi všlja.
	}

	public void keyPressed(KeyEvent e) {
		if (State == STATE.GAME) {

			if (e.getKeyCode() == KeyEvent.VK_UP) {
				player.setYVelocity(-13); // ehk siis kui klaviatuuril
											// vajutatakse up noolt, kutsutakse
											// všlja
				
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				player.setYVelocity(13);
			}
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				player.setXVelocity(-9); 
				
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				player.setXVelocity(9);
			}
		}
	}

	public void keyReleased(KeyEvent e) {
		if (State == STATE.GAME) {
			if (e.getKeyCode() == KeyEvent.VK_UP) {
				player.setYVelocity(0);
			} else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
				player.setYVelocity(0);
			}
			if (e.getKeyCode() == KeyEvent.VK_LEFT) {
				player.setXVelocity(0);
			} else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
				player.setXVelocity(0);
			}

		}

	}

	public void keyTyped(KeyEvent e) {
	}

	public void kasOnSurnud() {
		if (player.elud < 0) {
			State = STATE.MENU;
			player.elud = 3;
			player.punktid = 0;
			player.kasOnVasakul = false;

			player.x = 550;// et ta iga kord paremalt hakkaks ikka. ka pärast
							// surma saamist
			player.y = 200;
			player.xVelocity = 0;// oli probleem, et kui eelmise mängu lõõ
									// saabus, kui liikusin, hakkas
			// automaatselt edasi liikuma, see käsklus jätab ta seisma
		}
	}
}
