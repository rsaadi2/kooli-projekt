package pong2;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class Punkt {
	int height = 20;
	int width = 20;
	int x = 300;
	int y = 190;

	Random ran = new Random();

	public Punkt() {

	}

	public void update() {

	}

	public void paint(Graphics g) {

		g.setColor(Color.RED);
		g.fillOval(x, y, width, height);
	}

	public void checkCollusionWith(Player player) {
		if (this.x + width > player.getX() && this.x < player.getX() + player.width) {
			if (this.y + height > player.getY() && this.y < player.getY() + player.height) {
				
				player.punktid++;
				this.x = ran.nextInt(Pong2.window_width - 100) + 50;
				this.y = 50 + ran.nextInt(280); 
			}

		}
	}
}
