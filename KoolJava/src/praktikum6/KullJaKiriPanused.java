package praktikum6;

import lib.TextIO;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada panustega kulli ja kirja mäng.
 Kasutajale antakse 100 raha.
 Küsitakse panuse suurust, maksimaalne panus on 25 raha.
 Visatakse münt.
 Kui tuli kiri, saab kasutaja panuse topelt tagasi.
 Kui tuli kull, ei saa ta midagi.
 Mäng kestab seni, kuni kasutajal raha otsa saab.
 NB! Mõistlik on kasutajale raha jääki vahel näidata ka.

 Kopeeri teine ülesanne uude faili ning kirjuta sellele täiendus:
 Kirjutada panustega täringumäng.
 Kasutajalt küsitakse, millise täringu numbri peale ta panuse paneb (1..6), peale seda küsitakse panuse suurus (vahemikus 0 kuni palju kasutajal raha on).
 Veeretatakse täring. Kui kasutaja valis õige numbri, saab ta panuse 6-kordselt tagasi.
 */
public class KullJaKiriPanused {
    public static void main(String[] args) {

        int kasutajaRaha = 100;

        while(true) {

            int panuseSuurus = kasutajaSisestus(1, 25);
            kasutajaRaha -= panuseSuurus;

            int myndiVise = suvalineArv(0, 1);
            if (myndiVise == 0) {
                System.out.println("Võitsid, saad raha topelt tagasi!");
                kasutajaRaha += panuseSuurus * 2;
            } else {
                System.out.println("Kaotasid..");
            }
            System.out.println("Sul on " + kasutajaRaha + " eurot.");

            if (kasutajaRaha == 0) {

                break;

            }
        }
    }
    public static int suvalineArv(int min, int max) {

        // math.random väljastab nr-i 0-st 1-ni
        int vahemik = max - min + 1;
        return (int) (Math.random() * vahemik) + min;
    }

    public static int kasutajaSisestus(int min, int max) {

        int sisestus;
        do {
            System.out.println("Palun sisesta number vahemikus " + min + " kuni " + max);
            sisestus = TextIO.getlnInt();
        } while (sisestus < min || sisestus > max);
        return sisestus;
    }
}
