package praktikum6;

import lib.TextIO;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada liisu heitmise programm. Programm kysib 10 nime,
 * valib neist suvalise nime (randomiga) ja trükib välja.
 * Täiendus: Küsida esimese asjana inimeste arv.
 */
public class LiiskKymmeNime {
    public static void main(String[] args) {

        ArrayList<String> list = new ArrayList<String>();

        for (int i = 0; i < 5; i++) {

            System.out.println("Palun sisesta nimi:");
            String nimi = TextIO.getlnString();
            list.add(nimi);
        }
        int nr = suvalineArv(1,6);
        System.out.println(nr);
        System.out.println(list.get(nr - 1));
    }

    private static int suvalineArv (int min, int max){
        Random random = new Random();
        int arv = random.nextInt(max - min) + min;
        return arv;
    }

}
