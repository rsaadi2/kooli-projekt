package praktikum6;

import lib.TextIO;

import java.util.ArrayList;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada programm, mis küsib kasutajalt 10 arvu ning
 * trükib nad seejärel vastupidises järjekorras ekraanile.
 */
public class ArvTagurpidi {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < 10; i++) {
            System.out.print("Palun sisesta arv:");
            int arv = TextIO.getInt();
            list.add(arv);
        }
        for (int i = 9; i >= 0; i--) {

            System.out.print(" " + list.get(i));
        }
    }
}
