package praktikum6;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada meetod, mis tagastab etteantud massiivi elementide summa:
 public static int summa(int[] massiiv) {
 // meetodi sisu siia
 }
 public static void main(String[] args) {
 // sedasi saab meetodi välja kutsuda
 int summa = summa(new int[] {4, 3, 1, 7, -1});
 System.out.println(summa);
 }
 */
public class MassiiviElementideSumma {
    public static void main(String[] args) {
        // sedasi saab meetodi välja kutsuda
        int summa = summa(new int[] {4, 3, 1, 7, -1});
        System.out.println(summa);
    }

    public static int summa(int[] massiiv) {
        int sum = 0;
        for (int i = 0; i < massiiv.length; i++) {
            sum = sum + massiiv[i];
        }
        return sum;
    }
}
