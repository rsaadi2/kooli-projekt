package praktikum6;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada programm, mis loeb käsurealt käivitades nime ning trükib selle ekraanile.
 * Käsurea parameetrid on main meetodi sisendis (String[] args).
 * Käsurealt saab programmi käivitada näiteks sedasi:
 cd ~/Documents/workspace/Praktikumid/bin/
 java praktikum6.LoeNimi Marek
 */
public class LoeKasurealtNimi {
    public static void main(String[] args){


        if (args.length > 0){
            System.out.println(args[0]);
        }
    }
}
