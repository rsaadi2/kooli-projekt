package praktikum6;

import lib.TextIO;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada panustega kulli ja kirja mäng.
 Kasutajale antakse 100 raha.
 Küsitakse panuse suurust, maksimaalne panus on 25 raha.
 Visatakse münt.
 Kui tuli kiri, saab kasutaja panuse topelt tagasi.
 Kui tuli kull, ei saa ta midagi.
 Mäng kestab seni, kuni kasutajal raha otsa saab.
 NB! Mõistlik on kasutajale raha jääki vahel näidata ka.
 */

public class KullJaKiri {
    public static void main(String[] args) {

        int kasutajaRaha = 100;

        while (true) {
            System.out.println("hf");
            System.out.println("Mis panuseks paned?");
            int panus = TextIO.getlnInt();

            int suva;

            // Genereerime random numbri, 0 kuni 1
            suva = (int) (Math.random() * 2);

            if (suva == 0) {
                System.out.println("kiri");
                kasutajaRaha = kasutajaRaha + panus * 2;
                System.out.println("Őige, vőitsid juurde " + panus * 2 + " eurot");
            } else {
                System.out.println("kull");
                System.out.println("Kahjuks sa ei vőitnud");
                kasutajaRaha = kasutajaRaha - panus;
            }
            System.out.println("Sul on jä	rgi " + kasutajaRaha + " eurot");

        }

    }
}


