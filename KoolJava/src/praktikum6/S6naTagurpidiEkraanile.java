package praktikum6;

import lib.TextIO;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada programm, mis küsib kasutajalt sõna ja trükib selle tagurpidi ekraanile.
 */
public class S6naTagurpidiEkraanile {

    public static void main(String[] args) {

        System.out.println("Palun sisesta üks sőna:");
        String sona = TextIO.getlnString();

        for (int i = sona.length() - 1; i >= 0; i--) {

            System.out.print(sona.charAt(i));
        }
    }
}
