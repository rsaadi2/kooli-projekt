package praktikum6;

import lib.TextIO;

/**
 * Created by Kasutaja on 25.01.2016.
 *
 * Kirjutada äraarvamismäng.
 Arvuti "mõtleb" ühe arvu ühest sajani.
 Küsib kasutajalt, mis number oli.
 Kui kasutaja vastab valesti, siis ütleb kas arvatud number oli suurem või väiksem.
 Programm küsib vastust senikaua kuni kasutaja numbri õigesti ära arvab.
 */
public class ArvamisMang {
    public static void main(String[] args) {

        int arvutiArv = suvalineArv(1, 100);

        while (true) {
            System.out.println("Mis number on?");
            int kasutajaArv = TextIO.getlnInt();

            if (arvutiArv == kasutajaArv) {
                System.out.println("Tubli! Arvasid ära, Õige arv on " + arvutiArv + ".");
                break;
            } else if (kasutajaArv > arvutiArv) {
                System.out.println("Arvuti valitud arv on väiksem");
            } else {
                System.out.println("Arvuti valitud arv on suurem");
            }
        }
    }

    // teeme meetodi, mis tagastab suvalise arvu etteantud vahemikus

    public static int suvalineArv(int min, int max) {

        // math.random väljastab nr-i 0-st 1-ni
        int vahemik = max - min + 1;
        return (int) (Math.random() * vahemik) + min;
    }
}
