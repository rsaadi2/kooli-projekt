package lisa;

import lib.TextIO;

import java.applet.Applet;
import java.awt.*;

/**
 * Created by Kasutaja on 26.01.2016.
 */

public class Paike extends Applet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

	/*
	 * Ringjoone vo~rrand parameetrilisel kujul x = r * cos(t) y = r * sin(t) t
	 * = -PI..PI
	 */

    public void paint(Graphics g) {

        int w = getWidth();
        int h = getHeight();
        int x0 = w / 4; // Keskpunkt
        int y0 = h / 4;
        int r = 40; // Raadius
        int x, y;

        int n;

        do {

            System.out.print("Mitme kiirega päikest soovid? (Vali arv vahemikus 3-60) ");
            n = TextIO.getlnInt();

            if (n < 3 || n > 60) {
                System.out.println("Vale number");
            }
        } while (n < 3 || n > 60);

        int m = 360 / n;
        double t = Math.PI;

        g.setColor(Color.white);
        g.fillRect(0, 0, w, h);

        g.setColor(Color.yellow);

        x = (int) (r * Math.cos(t) + x0);
        y = (int) (r * Math.sin(t) + y0);
        g.fillOval(x0, y0, x0 * 2, y0 * 2);

        for (int a = 0; a <= 360; a += m) {

            x = (int) (Math.cos(Math.toRadians(a)) * 600) + w / 2;
            y = (int) (Math.sin(Math.toRadians(a)) * 600) + h / 2;
            g.drawLine(w / 2, h / 2, x, y);

        }
    }
}
