package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class AllaKeskm {
    public static void main(String[] args) {

        System.out.println(vaiksemad(new double[] {6., 32., 3., 41., 6., 12., 16.}));
    }

    private static double vaiksemad(double[] d) {
        int summa = 0;
        int keskm = 0;
        double arve = 0;

        for (int i = 0; i < d.length; i++) {
            summa = (int) (summa + d[i]);
        }
        keskm = summa/(d.length - 1);

        for (int i = 0; i < d.length; i++) {
            if (d[i] < keskm){
                arve = arve + 1;
            }
        }
        return arve;
    }

}
