package lisa;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class Nimed {
    public static void main(String[] args) {

        String[] naised = { "Tuuli", "Maali", "Juuli" };
        String[] mehed = { "Ants", "Jaak", "Teet" };

        Collections.shuffle(Arrays.asList(naised));

        for (int i = 0; i < mehed.length; i++) {

            System.out.println(mehed[i] + " + " + naised[i]);
        }

    }
}
