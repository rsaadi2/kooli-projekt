package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 *Koostada Java meetod, mis asendab suvalises parameetrina etteantud sőnes s kőik mittetähed märgiga \'*\'.
 */
public class S6naAsendus {

    public static void main (String[] args) {
        String s = "Tere, TUDENG, 1234!";
        String t = asenda (s); // \"Tere**TUDENG*******\"
        System.out.println (s + " > " + t);
    }

    public static String asenda (String s) {

        String vast = "";

        for(int i = 0; i < s.length(); i++)
        {
            char arv = s.charAt(i);
            if ( (64 <= arv & arv <= 90) || (97 <= arv & arv <= 122)){
                vast = vast + s.charAt(i);
            }
            else{
                vast = vast + "*";
            }

        }
        return vast; // TODO!!! Your code here
    }

}

