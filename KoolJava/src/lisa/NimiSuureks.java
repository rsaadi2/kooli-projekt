package lisa;

import lib.TextIO;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class NimiSuureks {
    public static void main(String[] args) {

        System.out.println("What is your name?");
        String nimi = TextIO.getlnString();

        System.out.println("Hello, " + nimi.toUpperCase() + ", nice to meet you!");
    }

}
