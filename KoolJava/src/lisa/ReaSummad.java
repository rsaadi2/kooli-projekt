package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class ReaSummad {
    public static void main(String[] args) {
        int[] res = reaSummad(new int[][] { { 1, 2, 3, -7 }, { 4, 5, 6 } }); // {6,
        // 15}
        System.out.println(Integer.toString(res[0]));
        System.out.println(Integer.toString(res[1]));
        // YOUR TESTS HERE
    }

    public static int[] reaSummad(int[][] m) {

        int[] summad = new int[m.length];

        for (int i = 0; i < m.length; i++) {
            int reasumma = 0;
            for (int j = 0; j < m[i].length; j++) {
                reasumma = reasumma + m[i][j];
            }
            summad[i] = reasumma;

        }

        // TODO!!! YOUR PROGRAM HERE
        return summad;
    }
}
