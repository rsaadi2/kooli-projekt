package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class V2ikesteAsendus {
    public static void main(String[] args) {

        String s = "Oh My GoD";
        System.out.println(asendaja(s));

    }

    private static String asendaja(String s){
        String tulemus = " ";

        for (int i = 0; i < s.length(); i++) {

            char taht = s.charAt(i);

            if (Character.isLowerCase(taht)){

                tulemus = tulemus + "-";
            }else{
                tulemus = tulemus + taht;
            }

        }

        return tulemus;
    }
}
