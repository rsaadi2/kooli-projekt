package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostada Java meetod, mis asendab parameetrina etteantud sőnes s kőik numbrid märgiga \'#\'.
 */
public class NumbriteAsendus {
    public static void main (String[] args) {
        String s = "Tere, 1234 ja 5678";
        String t = asenda (s); // \"Tere, #### ja ####\"
        System.out.println (s + " --> " + t);
    }

    public static String asenda (String s) {
        return s.replaceAll("[0-9]", "#"); // TODO!!! Your code here
    }

}


