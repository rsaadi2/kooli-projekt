package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class RuutudeSumma {
    public static void main(String[] args) {

        int res = summa(new int[] {9, 2, 3, 4, 5, 3});
        System.out.println(res);
    }

    private static int summa(int[] m) {
        int sum = 0;
        for (int i = 0; i < m.length; i++) {
            sum = (int) (sum + Math.pow(m[i], 2));
        }

        return sum;
    }
}
