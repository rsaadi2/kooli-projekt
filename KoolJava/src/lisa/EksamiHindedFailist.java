package lisa;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class EksamiHindedFailist {
    public static void main(String[] args) throws IOException {

        String name;
        int score, totalScore, count;
        int avg;

        @SuppressWarnings("resource")
        Scanner inputFile = new Scanner(new File("testdata.txt"));

        name = inputFile.next();
        System.out.println(name);

        score = 0;
        totalScore = 0;
        count = 0;

        for (int i = 0; i < 3; i++) {
            score = inputFile.nextInt();
            System.out.println(score);
            totalScore = totalScore + score;
            count++;
        }
        avg = totalScore / count;
        System.out.println("Keskmine: " + avg);
    }

}
