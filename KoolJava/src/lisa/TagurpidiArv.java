package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class TagurpidiArv {
    /*
	 * On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab täisarvu,
	 * mis saadakse n kümnendesituses numbrite järjekorra ümberpööramise teel.
	 * Integer n is positive. Write a Java method to find an integer that consists of
	 * decimal digits of n in inverse order.
	 */

    public static void main (String[] args) {
        System.out.println (inverse (1234)); // 4321
    }

    public static int inverse (int n) {
        String arv = Integer.toString(n);
        String tagurpidi = "";

        int index = 0;
        for (int i=0; i<arv.length(); i++) {
            index = (arv.length()-i)-1;
            tagurpidi = tagurpidi + arv.charAt(index);
        }
        return Integer.parseInt(tagurpidi); // TODO!!! Your code here!
    }
}

