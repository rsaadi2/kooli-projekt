package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class VeelTagurpidiKymned {
    public static void main(String[] args) {

        System.out.println(tagurpidi(123456));

    }

    private static int tagurpidi(int nr) {

        int tulemus = 0;
        int jaak = 0;

        do {

            jaak = nr % 10;
            tulemus = tulemus * 10 + jaak;
            nr = nr / 10;

        } while (nr > 0);
        return tulemus;

    }
}
