package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class Random {
    public static void main(String[] args) {
        int tulemus = 0;

        for (int i = 0; i < 10; i++) {

            int nr = (int) (Math.random() * 6) + 1;

            System.out.println("The first die comes up " + nr);

            tulemus += nr;

            System.out.println("Your total roll is " + tulemus);
        }
    }
    // Imp: FORTRAN; ALGOL; COBOL; BASIC; Pascal; C, Ada
    // Funkts: Lisp, ML, Haskell, Scheme
    // Loogilised: Prolog
    // OOP: Smalltalk, Simula, C++, Java
}
