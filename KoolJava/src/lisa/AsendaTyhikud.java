package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class AsendaTyhikud {

	/*
	 *
	 * Koostada Java meetod, mis asendab parameetrina etteantud sõnes s kõik tühikud märgiga '-'.
	 * Write a Java method to replace all spaces in a given string s by symbol '-'.
	 */

    public static void main (String[] args) {
        String s = "Tere, TUDENG,   tore ARVUTI sul!";
        String t = asenda (s); // "Tere,-TUDENG,---tore-ARVUTI-sul!"
        System.out.println (s + " > " + t);
    }

    public static String asenda (String s) {
        return s.replaceAll(" ", "-"); // TODO!!! Your code here
    }
}
