package lisa;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class Tulumaks {
    public static void main(String[] args) {

        double palk = 155;
        double miinimum = 154;
        double maar = 0.20;
        double tulumaks = 0;

        if (palk > miinimum) {
            tulumaks = (palk - miinimum) * maar * 12;
        }
        else {
            tulumaks = 0;
        }
        System.out.println("Tulumaksu suurus aastas on " + tulumaks);

    }
}
