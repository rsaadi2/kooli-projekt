package praktikum8;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis küsib kasutaja käest kümme sõna ja trükib välja sõna pikkuse ja sõna enda.
 Näiteks:
 4 kala
 13 koerakoonlane
 6 lammas
 8 Voldemar
 */
public class KymmeS6naPikkusTryki {

    public static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args) {
        ArrayList<String> nimedeList = new ArrayList<String>();

        System.out.println("Palun sisesta kümme sõna:");

        while (nimedeList.size() < 10) {
            System.out.print((nimedeList.size() + 1) + ": ");
            nimedeList.add(getName());
        }

        System.out.println("\nSõnade pikkused ja nimed: ");
        for (String element : nimedeList) {
            System.out.println(element.length() + " " + element);
        }
        userInput.close();
    }

    private static String getName() {
        String nimi = " ";
        try {
            nimi = userInput.next();
        } catch (InputMismatchException e) {
        }
        return nimi;
    }
}

