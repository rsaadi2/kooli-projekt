package praktikum8;

import lib.TextIO;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis küsib kasutajalt sõna ja kontrollib, kas see on edaspidi ja
 * tagurpidi lugedes sama (palindroom).
 * Näited sellistest sõnadest:
 * udu  kook  meem kuulilennuteetunneliluuk
 */
public class KontrolliPalindroomi {
    public static void main(String[] args) {

        System.out.println("Sisesta sőna:");
        String sona = TextIO.getlnString();
        String tagurpidi = "";
        Character taht;

        for (int i = 0; i < sona.length(); i++) {
            taht = sona.charAt(sona.length() - 1 - i);
            tagurpidi = tagurpidi + taht;
        }

        System.out.println(tagurpidi);

        if (sona.equals(tagurpidi)) {
            System.out.println("See ongi palindroom");
        } else {
            System.out.println("See pole palindroom");
        }
    }
}
