package praktikum8;

import java.util.Random;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjuta meetod, mis genereerib sellise kahemõõtmelise massiivi,
 * mille elemendid on suvalised numbrid vahemikus 0 kuni 100.
 * Tagastatava massiivi suurus antakse meetodile parameetritega ette.
 * Meetodi signatuur võib olla näiteks selline:
 publiv static int[][] generateMatrix(int width, int heigth)
 */
public class KaksDMassiivSuvalisedNullKuniSada {
    public static void main(String[] args) {

        int[][] maatriks = generateMatrix(5, 3);
        prindiMaatriks(maatriks);
    }

    public static int[][] generateMatrix(int width, int heigth) {

        int[][] res = new int[heigth][width];

        for (int i = 0; i < heigth; i++) {
            for (int j = 0; j < width; j++) {
                Random random = new Random();
                int arv = random.nextInt(101);
                res[i][j] = arv;
            }
        }

        return res;
    }

    public static void prindiMaatriks(int[][] maatriks) {
        for (int[] rida : maatriks) {
            for (int veerg : rida) {
                System.out.printf("%4d", veerg);
            }
            System.out.println();
        }
    }
}
