package praktikum8;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjuta meetod, mis tagastab kahemõõtmelise massiivi suurima ja väikseima elemendi vahe.
 * Meetodi võimalik signatuur:
 public static int maxAndMinDifference(int[][] matrix)
 */
public class KaksDMassiivSuurVaikeElemendiVahe {
    public static void main(String[] args) {

        System.out.println(maxAndMinDifference(new int[][]{{4, 2, 4, 5},{6, 55, 8, 5},{4, 5, 9, 2}}));
    }

    public static int maxAndMinDifference(int[][] matrix){

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int vahe = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if (matrix[i][j] < min){
                    min = matrix[i][j];
                } else if (matrix[i][j] > max){
                    max = matrix[i][j];
                }
            }
        }
        vahe = max - min;

        return vahe;
    }
}
