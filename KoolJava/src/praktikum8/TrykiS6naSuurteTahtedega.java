package praktikum8;

import lib.TextIO;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis küsib kasutaja käest sõna ning trükib välja selle sõna suurte tähtedega ning tähed sidekriipsudega eraldatud.
 Näiteks, kui kasutaja sisestab "Teretore", trükib programm "T-E-R-E-T-O-R-E".
 */
public class TrykiS6naSuurteTahtedega {
    public static void main(String[] args) {

        System.out.println("Palun sisesta mingi sőna:");
        String sona = TextIO.getlnString();

        String uussona = sona.toUpperCase();

        for (int i = 0; i < sona.length(); i++) {

            if (i < sona.length() - 1){
                System.out.print(uussona.charAt(i) + "-");
            }else{
                System.out.print(uussona.charAt(i));
            }
        }
    }
}
