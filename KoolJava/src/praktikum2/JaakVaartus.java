package praktikum2;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada programm, mis küsib inimeste arvu ja grupi suuruse ning väljastab mitu gruppi
 * nendest inimestest moodustada
 * saab (täisarvuline jagatis) ja mitu inimest üle jääb
 * (jagatise jäägi leidmise jaoks on eraldi tehte-märk: %).
 */
public class JaakVaartus {
    public static void main(String[] args) {
        int inim, grupp, arv, yle;
        System.out.println("mitu inimest kokku");
        inim = TextIO.getInt();
        System.out.println("mitu inimest mahub gruppi");
        grupp = TextIO.getInt();
        arv = inim / grupp;
        yle = inim % grupp;
        System.out.println("saab moodustada " + arv + " rühma");
        System.out.println("ja üle jääb " + yle + " inimest.");
    }
}
