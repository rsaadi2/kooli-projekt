package praktikum2;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada programm, mis küsib kasutajalt kaks arvu ja väljastab nende korrutise.
 */
public class Korrutis {
    public static void main(String[] args) {
        int arv1, arv2, korrutis;
        System.out.println("siseta arv!");
        arv1 = TextIO.getInt();
        System.out.println("sisesta teine arv!");
        arv2 = TextIO.getInt();
        korrutis = arv1 * arv2;
        System.out.println("nende arvude korrutis on " + korrutis);
    }
}

