package praktikum2;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada programm, mis küsib kasutajalt nime ning annab teada, mitu tähte nimes on.
 String nimi = "Kalle";
 int nimePikkus = nimi.length();
 System.out.println(nimePikkus);
 System.out.println("neli".length());
 */
public class TahedNimes {
    public static void main(String[] args) {
        String nimi;
        String nimi1 = "Malle";
        System.out.println("mis su nimi on?");
        nimi = TextIO.getlnString();
        int nimePikkus = nimi.length();
        System.out.println(nimePikkus);
        System.out.println("Malles on " + nimi1.length() + " tähte");
        System.out.println("Andruse nimes on " + "Andrus".length() + " tähte");
    }
}
