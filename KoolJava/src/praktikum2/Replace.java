package praktikum2;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada programm, mis küsib kasutaja käest tekstisisestuse,
 * asendab selles kõik 'a'-tähed altkriipsudega ('_') ning trükib tulemuse konsooli.
 "kala".replace('k', 's')
 */
public class Replace {
    public static void main(String[] args) {
        String sisestus, uus;
        System.out.println("palun sisesta sõna!");
        sisestus = TextIO.getln();
        uus = sisestus.replaceAll("a", "_");
        System.out.println(uus);
    }


}
