package praktikum2;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 */
public class NimiVanus {
    public static void main(String[] args) {
        String nimi;
        System.out.println("mis su nimi on?");
        nimi = TextIO.getln();
        System.out.println("Su nimi on " + nimi);
        int vanus, uusVanus;
        System.out.println("kui vana oled?");
        vanus = TextIO.getInt();
        System.out.println("sa oled " + vanus + " aastat vana.");
        uusVanus = vanus + 5;
        System.out.println("Viie aasta pärast oled " + uusVanus + " aastat vana");

    }

}
