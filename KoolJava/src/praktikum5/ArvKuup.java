package praktikum5;

import lib.TextIO;
/**
 * Created by Kasutaja on 22.01.2016.
 */
public class ArvKuup {
    public static void main(String[] args) {
        System.out.println("anna arv");
        int arv = TextIO.getInt();
        int arvKuubis = kuup(arv);
        System.out.println(arvKuubis);
    }
    public static int kuup(int sisestus){
        int tagastus = (int)Math.pow(sisestus,3);
        return tagastus;
    }
}
