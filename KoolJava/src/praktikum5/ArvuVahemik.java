package praktikum5;

import lib.TextIO;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada meetod, mis saab parameetritena kaks arvu (vahemiku)
 * ning tagastab kasutaja sisestatud arvu,
 * juhul kui see on lubatud vahemikus. Senikaua,
 * kui kasutaja sisestab midagi ebasobivat,
 * käseb meetod kasutajal arvu uuesti sisestada.
 Selle meetodi signatuur võib olla näiteks järgmine:
 public static int kasutajaSisestus(int min, int max)
 */
public class ArvuVahemik {
    public static void main(String[] args) {

        int kasutajaSisestas = kasutajaSisestus(1, 10);
        System.out.println("Tubli, arv " + kasutajaSisestas + " jääb sellesse vahemikku.");
    }

    public static int kasutajaSisestus(int min, int max) {
        while (true) {

            System.out.println("Palun sisesta üks arv vahemikus " + min + " kuni " + max);
            int sisestus = TextIO.getInt();
            if (sisestus >= min && sisestus <= max) {
                return sisestus;
            } else {
                System.out.println("See arv ei jää lubatud vahemikku.");
            }
        }
    }
}