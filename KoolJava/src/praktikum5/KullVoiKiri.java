package praktikum5;

import lib.TextIO;

import java.util.Random;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada mäng kull ja kiri.
 * Programm küsib kasutajalt: "kull või kiri?". "Viskab" mündi ja teatab,
 * kas kasutaja arvas õigesti või mitte.
 * Vihje: ära hakka jändama mingite stringidega, küsi kasutajalt number;
 * kasuta selleks eelnevalt valmis kirjutatud meetodit. Meetodit võib
 * täiendada ühe String-tüüpi parameetriga, et kasutajale trükitaks
 * välja sobilik lause. Meetodi signatuur oleks siis näiteks järgmine:
 * public static int kasutajaSisestus(String kysimus, int min, int max)
 */
public class KullVoiKiri {
    public static void main(String[] args) {

        int vastus = kasutajaSisestus("Kas kull(0) vői kiri(1)?", 0, 2);
        System.out.println("Tubli, vastus oli " + vastus);
    }

    public static int kasutajaSisestus(String kysimus, int min, int max) {

        while (true) {

            Random random = new Random();
            System.out.println(kysimus);
            int kasutajaVastus = TextIO.getInt();

            int arvutiVastus = random.nextInt(max - min) + min;

            if (kasutajaVastus == arvutiVastus) {
                return kasutajaVastus;
            } else {
                System.out.println("Proovi uuesti.");
            }
        }
    }
}

