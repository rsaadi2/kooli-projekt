package praktikum5;

/**
 * Created by Kasutaja on 25.01.2016.
 * On antud risttahukakujuline telliskivi mõõtmetega a, b, c ning ristkülikukujuline ava
 * mõõtmetega x, y (kõik suurused on positiivsed reaalarvud). Koostage programm, mis teeb
 * kindlaks, kas kivi mahub läbi ava nii, et kivi tahk libiseb mööda ava serva
 * (s.t. kivi ei tohi olla "viltu").
 * public static boolean mahub (double a, double b, double c, double x, double y)
 *
 */
public class Tellis {

    public static void main (String[] param) {
    }

    public static boolean mahub (double a, double b, double c, double x, double y) {
        if((a <= x && b <= y) || (a <= y && b <= x)){
        // külg ab mahub läb
        return true;
    }
    else if((c <= x && b <= y) || (c <= y && b <= x)){
        // külg bc mahub läbi
        return true;
    }
    else if((a <= x && c <= y) || (a <= y && c <= x)){
        // külg ac mahub läbi
        return true;
    }
    else return false;
    }
}
