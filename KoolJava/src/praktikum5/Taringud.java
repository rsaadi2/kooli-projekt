package praktikum5;

import java.util.Random;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada programm, mis viskab 5 täringut ning arvutab silmade summa
 NB! Lahendus peab olema tehtud tsükliga.
 Vihje: tsüklis summa arvutamiseks tuleb summat sisaldav muutuja
 enne tsükli käivitamist väärtustada nulliga
 ja tsükli kehas liita sellele üks haaval väärtusi otsa (sum = sum + ...)
 */
public class Taringud {
    public static void main(String[] args) {

        Random random = new Random();
        int silmadeSumma = 0;
        for (int i = 0; i < 5; i++) {

            int silm = random.nextInt(6) + 1;
            System.out.println(silm);
            silmadeSumma = silmadeSumma + silm;

        }
        System.out.println(silmadeSumma);
    }
}
