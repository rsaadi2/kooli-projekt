package praktikum5;

import java.util.InputMismatchException;
import java.util.Scanner;
/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada liisu tõmbamise programm.
 Küsida kasutajalt inimeste arv.
 Väljastada random number vahemikus 1 kuni arv (kaasaarvatud)
 NB! Kontrollida, et töötab õigesti: st. öeldes mitu korda järjest arvuks 3,
 peab võimalike vastuste hulgas olema nii ühtesid, kahtesid kui kolmi.
 *
 */
public class Liisk {
    public static void main(String[] args) {
       // public static void main (String[]args){
            System.out.println("liisu");
            System.out.print("Sisesta inimeste arv: ");
            int inimesteArv = lib.TextIO.getlnInt();
            System.out.println("Liisu on " + valiLiisu(inimesteArv));
        }

    public static int juhuArv(int min, int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    public static int valiLiisu(int inimesteArv) {
        int liisu = juhuArv(1, inimesteArv);
        return liisu;
    }
}
