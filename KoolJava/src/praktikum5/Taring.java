package praktikum5;

import java.util.Random;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjutada täringumäng:
 Programm viskab kaks täringut mängijale ja kaks täringut endale (arvutile),
 arvutab mõlema mängija silmade summad ja teatab, kellel oli rohkem.
 */
public class Taring {
    public static void main(String[] args) {
        int kasutajaTaringud = viskaTaringut() + viskaTaringut();
        int arvutiTaringud = viskaTaringut() + viskaTaringut();

        System.out.printf("Arvuti viskas %d punkti%nKasutaja viskas %d punkti%n",
                kasutajaTaringud, arvutiTaringud);

        if (kasutajaTaringud > arvutiTaringud) {
            System.out.println("Kasutaja võitis");
        } else if (kasutajaTaringud < arvutiTaringud) {
            System.out.println("Arvuti võitis");
        } else if (kasutajaTaringud == arvutiTaringud) {
            System.out.println("Viik");
        } else {
            System.out.println("Seda ei tohiks juhtuda");
        }
    }
    public static int randomRange(int min, int max) {
        return min + (int)(Math.random() * ((max - min) + 1));
    }

    public static int viskaTaringut() {
        return randomRange(1, 6);
    }

}
