package KT2;

/**
 * Created by Kasutaja on 27.01.2016.
 * Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda n täisarvumaatriksi,
 * mille iga elemendi väärtuseks on selle elemendi reaindeksi ja veeruindeksi summa ruut (indeksid algavad nullist).
 * public static int[][] muster (int n)
 */
public class MaatriksReaJaVeeruIndeksSummaRuut {
    //public class Answer {

    public static void main(String[] args) {
        int[][] res = muster(9);
        for (int[] row : res) {
            for (int col : row) {
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }

    public static int[][] muster(int n) {
        int[][] maatriks = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                maatriks[i][j] = (int) Math.pow(i + j, 2);
            }
        }

        return maatriks;
    }
}

