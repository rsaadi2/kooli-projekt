package KT2;

/**
 * Created by Kasutaja on 27.01.2016.
 * Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda
 * n täisarvumaatriksi, mille iga elemendi väärtuseks on selle elemendi reaindeksi ja
 * veeruindeksi summa (indeksid algavad nullist).
 public static int[][] liitmisTabel (int n)
 */
public class MaatriksReaJaVeeruSumma {
    //public class Answer {

    public static void main (String[] args) {
        int[][] res = liitmisTabel (9);

        for (int[] row : res) {
            for (int col : row) {
                System.out.printf("%3d ", col);
            }
            System.out.println();
        }
    }

    public static int[][] liitmisTabel (int n) {
         int[][] m = new int[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    m[i][j] = i + j;
                }
            }

            return m;
    }

}
