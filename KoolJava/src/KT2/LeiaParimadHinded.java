package KT2;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class LeiaParimadHinded {
    //public class Answer {

    public static void main(String[] args) {
        int[][] hinded = {
                {0, 2, 5, 3, 1},
                {5, 4, 3, 2, 1, 3},
                {5, 5, 5},
                {5, 5, 4, 4},
                {5, 5, 3}
        };

        leiaParimad(hinded);
    }

    public static int[] leiaParimad(int[][] h) {
        int[] pingerida = new int[h.length];

        ArrayList<Integer[]> keskmised = new ArrayList<>();

        for (int i = 0; i < h.length; i++) {
            int[] opilane = h[i];
            double keskmine = 0;
            for (int j = 0; j < opilane.length; j++) {
                keskmine += opilane[j];
            }
            keskmine = keskmine / opilane.length;
            keskmised.add(new Integer[]{i, (int) keskmine});
        }

        Collections.sort(keskmised, new Comparator<Integer[]>() {
            @Override
            public int compare(Integer[] i1, Integer[] i2) {
                if (i1[1] > i2[1]) {
                    return -1;
                } else if (i1[1] < i2[1]) {
                    return 1;
                } else {
                    if (i1[0] < i2[0]) {
                        return 1;
                    } else if (i1[0] < i2[0]) {
                        return -1;
                    } else {
                        return 0;
                    }
                }
            }
        });

        for (int i = 0; i < keskmised.size(); i++) {
            pingerida[i] = keskmised.get(i)[0];
            System.out.printf("index=%d avgGrade=%d%n", keskmised.get(i)[0], keskmised.get(i)[1]);
        }

        return pingerida;
    }

}
