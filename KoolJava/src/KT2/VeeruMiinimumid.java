package KT2;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class VeeruMiinimumid {
    public static void main(String[] args) {
        veeruMinid(new int[][] {{1,2,3},{1,5},{5,8,4,8,9}});

        //new int[][]{{1, 2, 6}, {-4, 5, 3}} // return.length=3, return array peab olema {-4, 2, 3}
        veeruMinid(new int[][]{{1, 2, 6}, {-4, 5, 3}});

        //new int[][]{{1, 2, 0}, {4, 0, 0}}  // return.length=3
        veeruMinid(new int[][]{{1, 2, 0}, {4, 0, 0}});

        //new int[][]{{1}, {4, 5, 6}}  // return.length=3
        veeruMinid(new int[][]{{1}, {4, 5, 6}});
    }

    // leiab 2D masiivi iga veeru minimummi ja tagastab need
    public static int[] veeruMinid(int[][] m) {
        int min_size = 0;

        for (int[] row : m){
            if (row.length > min_size){
                min_size = row.length;
            }
        }

        // loome miinimummide salvestamiseks massiivi mis on sama pikk kui ette antud masiiv
        int[] minimummid = new int[min_size];


        for (int i = 0; i < minimummid.length; i++) {
            int min = Integer.MAX_VALUE;  // antud rea väikseim arv
            for (int x = 0; x < m.length; x++) {
                if (m[x].length > i && m[x][i] < min) {
                    min = m[x][i];
                    System.out.printf("%3d", min);  // prindime välja meie sisendi
                }
            }
            // pistame leitud minimummi meie minimumme hoidvasse arraysse
            minimummid[i] = min;
            // prindime uue rea ja leitud minimummi
            System.out.printf("%n min on:%3d%n", min);
        }

        System.out.print("minimummid on: ");
        for (int el : minimummid) {
            System.out.print(el+", ");
        }
        System.out.println();

        return minimummid;
    }
}
