package KT2;

import java.util.Arrays;

/**
 * Created by Kasutaja on 27.01.2016.
 * Koostage Java meetod etteantud täisarvumaatriksi m veerusummade massiivi leidmiseks
 * (massiivi j-s element on maatriksi j-nda veeru summa). Arvestage, et m read võivad olla erineva pikkusega.
    public static int[] veeruSummad (int[][] m)
 */
public class VeeruSummadeMassiiv3 {
//    public class Answer {

        public static void main(String[] args) {
            int[] res = veeruSummad (new int[][] { {1,2,3}, {4,5,6} }); // {5, 7, 9}
            System.out.println(Arrays.toString(res));
        }

        public static int[] veeruSummad(int[][] m) {
            int max = 0;
            for (int[] row : m){
                if (row.length > max)
                    max = row.length;
            }
            int[] summa = new int[max];
            for (int rida = 0; rida < m.length; rida++) {
                for (int veerg = 0; veerg < m[rida].length; veerg++) {
                    summa[veerg] = summa[veerg] + m[rida][veerg];
                }
            }
            return summa;
        }

    }

