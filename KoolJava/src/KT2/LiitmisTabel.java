package KT2;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class LiitmisTabel {
    public static void main(String[] args) {
        int[][] res = liitmisTabel(9);

        for (int[] row : res) {
            for (int col : row) {
                System.out.printf("%3d ", col);
            }
            System.out.println();
        }
    }

    public static int[][] liitmisTabel(int n) {
        int[][] m = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                m[i][j] = i + j;
            }
        }

        return m;
    }

}
