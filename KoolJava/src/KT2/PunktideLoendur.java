package KT2;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class PunktideLoendur {
    //public class Answer {

    public static void main(String[] args) {
        System.out.println("score on: " + score(new int[] { 4, 1, 2, 3, 0 }) + ", peaks olema 9"); // 9
        System.out.println("score on: " + score(new int[] { -2, -1, -1, 0 }) + ", peaks olema -1"); // -1
        System.out.println("score on: " + score(new int[] { 3, 6, 2, 8, 4 }) + ", peaks olema 18"); // 18
        System.out.println("score on: " + score(new int[] { 9, 1, 1, 3 }) + ", peaks olema 12"); // 12
    }
    public static int score(int[] points) {
        // prindime välja meile ette antud masiivi
        System.out.print("sisend:");
        for (int el : points) {
            System.out.printf("%3d", el);
        }
        System.out.println();

        int score = 0;  // palju lõpuks punkte koos on

        int min1 = Integer.MAX_VALUE; // array väikseim arv
        int min1_i = 0; // mis indexi peal ta asub

        int min2 = Integer.MAX_VALUE;  // array suuruselt järgmine väikseim arv, juhul kui min1 arve on kaks või rohkem siis min1 == min2
        int min2_i = 0;  // kus kohas see teine arv asub arrays

        if (points.length < 3) {  // katseid kokku peab olema rohkem kui 2
            return 0;  // katseid oli vähem kui 3, punktisumma on järelikult 0
        }

        // leiame kõige väiksema arvu
        for (int i = 0; i < points.length; i++) {
            int point = points[i];
            if (point < min1) {
                min1 = point;
                min1_i = i;
            }
        }
        System.out.println("min1 arv on "+min1+" indeksiga "+min1_i);

        // leiame suuruselt järgmise arvu
        for (int i = 0; i < points.length; i++) {
            int point = points[i];
            if (point < min2 && i != min1_i) {
                min2 = point;
                min2_i = i;
            }
        }
        System.out.println("min2 arv on "+min2+" indeksiga "+min2_i);

        // arvutame kokku punkti summa
        for (int i = 0; i < points.length; i++) {
            int point = points[i];
            if (i != min1_i && i != min2_i) {
                score += point;
            }
        }

        return score;
    }
}
