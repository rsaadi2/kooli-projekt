package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis genereerib parameetritena etteantud suurusega maatriksi,
 * kus iga element on rea ja veeruindeksi summa kahega jagamise jääk. Indeksid algavad nullist.
 public static int[][] kahegaJaakMaatriks(int ridu, int veerge)
 Näiteks parameetrite väärtustega 3 ja 5 peaks tulemus olema selline:
 0 1 0 1 0
 1 0 1 0 1
 0 1 0 1 0
 */
public class GenereeriEtteantudsuurusegaMaatriks {
    public static void main(String[] args) {
        tryki(kahegaJaakMaatriks(3, 5));
        tryki(kahegaJaakMaatriks(2, 5));
    }

    public static void tryki(int[][] maatriks){

        for(int[] row : maatriks){
            for(int el : row){
                System.out.print(el + " ");
            }
            System.out.println();
        }
    }
    public static int[][] kahegaJaakMaatriks(int ridu, int veerge){

        int[][] maatriks = new int[ridu][veerge];
        for (int i = 0; i < ridu; i++){
            for (int j = 0; j < veerge; j++){
                maatriks[i][j] = (i + j) % 2;
            }
        }
        return maatriks;
    }
    /*
    public static int[][] kahegaJaakMaatriks(int ridu, int veerge) {
        System.out.println();

        int[][] temp = new int[ridu][veerge];

        for (int i = 0; i < ridu; i++) {
            for (int j = 0; j < veerge; j++) {
                temp[i][j] = (i + j) % 2;

            }
        }
        return temp;
    } */
}
