package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Arvutada maatriksi iga rea elementide summa
 * public static int[] ridadeSummad(int[][] maatriks)
 */
public class MaatriksiIgaReaElementideSumma {
    public static void main(String[] args) {
        int[][] maatriks2D = {{1, 1, 1, 1, 1}, {2, 2, 2, 2, 2}, {3, 3, 3, 3, 3}, {4, 5, 6, 7, 8},
                {5, 6, 7, 8, 9},};
        tryki(ridadeSummad(maatriks2D));
        // tryki(ridadeSummad(maatriks));
    }

    public static void tryki(int[] massiiv){
        System.out.println("Ylesanne 1:");
        for(int element : massiiv){
            System.out.print(element + " ");
        }
    }

    /**
     * Arvutada maatriksi iga rea elementide summa
     *
     * @param maatriks
     * @return
     */
    public static int[] ridadeSummad(int[][] maatriks) {
        System.out.println("\nÜlesanne 3:");
        //loon massiivi
        int[] sums = new int[maatriks.length];
        //iga rea kohta tee seda;
        for (int i = 0; i < maatriks.length; i++) {
            //iga i rea elemendi kohta tee seda:
            for (int el : maatriks[i]) {
                //liida juurde summade massiivi i elemendile
                sums[i] += el;
            }
        }
        return sums;
    }

    /*
    public static int[] ridadeSummad(int[][] maatriks) {
        System.out.println();
        int[] summad = new int[maatriks.length];
        for (int i = 0; i < maatriks.length; i++) {

            summad[i] = reaSumma(maatriks[i]);
        }
        return summad;
    }

    public static int reaSumma(int[] rida) {
        int summa = 0;
        for (int i : rida) {
            summa += i;
        }
        return summa;
    } */
}
