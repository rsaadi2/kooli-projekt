package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Leida iga rea suurim element.
 * public static int[] ridadeMaksimumid(int[][] maatriks)
 * <p>
 * Leida kogu maatriksi väikseim element.
 * public static int miinimum(int[][] maatriks)
 */
public class IgaReaSuurimVaikseimElement {
    public static void main(String[] args) {
        int[][] maatriks2D = {{1, 1, 1, 1, 1}, {2, 2, 2, 2, 2}, {3, 3, 3, 3, 3}, {4, 5, 6, 7, 8},
                {5, 6, 7, 8, 9},};
        int[][] another = {{1, 2}, {3, 4}, {5, 6}};
        tryki(ridadeMaksimumid(maatriks2D));
        tryki(ridadeMaksimumid(another));

        System.out.println(koguMiinimum(maatriks2D));
        System.out.println(koguMiinimum(another));
    }

    public static void tryki(int[] massiiv){
        System.out.println("Ylesanne 1:");
        for(int element : massiiv){
            System.out.print(element + " ");
        }
    }


    public static int[] ridadeMaksimumid(int[][] maatriks) {
        System.out.println();

        int[] maxid = new int[maatriks.length];

        for (int i = 0; i < maatriks.length; i++) {
            int max = Integer.MIN_VALUE;

            for (int el : maatriks[i]) {
                if (el > max) {
                    max = el;
                }
            }
            maxid[i] = max;
        }
        return maxid;
    }

    public static int koguMiinimum(int[][] maatriks) {
        System.out.println();

        int min = Integer.MAX_VALUE;

        for (int[] rida : maatriks) {
            for (int el : rida) {
                if (el < min) {
                    min = el;
                }
            }
        }
        return min;
    }
}
