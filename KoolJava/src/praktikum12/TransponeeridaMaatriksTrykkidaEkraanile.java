package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Transponeerida allpool toodud maatriks ja trükkida tulemus ekraanile.
 1 2
 3 4
 5 6

 public static int[][] transponeeri(int[][] maatriks)
 */
public class TransponeeridaMaatriksTrykkidaEkraanile {

    public static void main(String[] args) {
        int[][] maatriks2D = { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 }, { 3, 3, 3, 3, 3 }, { 4, 5, 6, 7, 8 },
                { 5, 6, 7, 8, 9 }, };
        int[][] another = {{1,2},{3,4},{5,6}};

        tryki(transponeeri(maatriks2D));
        tryki(transponeeri(another));
    }

    public static void tryki(int[][] maatriks){
        System.out.println("Ülesanne 2:");
        for(int[] row : maatriks){
            for(int el : row){
                System.out.print(el + " ");
            }
            System.out.println();
        }
    }

    public static int[][] transponeeri(int[][] maatriks){
        System.out.println("\nÜlesanne 8:");
        int[][] maatriks2 = new int[maatriks.length][maatriks[0].length];

        for (int row = 0; row < maatriks.length; row++){
            for (int col = 0; col < maatriks[row].length; col++){
                maatriks2[col][row] = maatriks[row][col];
            }
        }
        return maatriks2;
    }

}
