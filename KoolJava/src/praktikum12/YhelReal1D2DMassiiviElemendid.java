package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada meetod, mis trükib ekraanile ühel real parameetrina etteantud ühemõõtmelise täisarvumassiivi elemendid
 public static void tryki(int[] massiiv)
 Kirjutada meetod, mis trükib ekraanile parameetrina etteantud kahemõõtmelise massiivi (maatriksi)
 public static void tryki(int[][] maatriks)
 Kasuta maatriksi rea trükkimiseks kindlasti eelnevalt kirjutatud meetodit!
 */
public class YhelReal1D2DMassiiviElemendid {

    public static void main(String[] args) {

        int[] massiiv = { 4, 6, 6, 1, 3, 5, 6, 7, 5, 8 };
        tryki(massiiv);

        int[][] maatriks2D = { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 }, { 3, 3, 3, 3, 3 }, { 4, 5, 6, 7, 8 },
                { 5, 6, 7, 8, 9 }, };
        tryki(maatriks2D);
        //int[][] another = {{1,2},{3,4},{5,6}};

    }
    /**
     * Kirjutada meetod, mis trükib ekraanile ühel real parameetrina etteantud ühemõõtmelise täisarvumassiivi elemendid
     * @param massiiv
     */
    public static void tryki(int[] massiiv) {

        for (int i = 0; i < massiiv.length; i++)
            System.out.print(massiiv[i] + " ");
    }
    /**
     * Kirjutada meetod, mis trükib ekraanile parameetrina etteantud kahemõõtmelise massiivi (maatriksi)
     * @param maatriks
     */
    public static void tryki(int[][] maatriks) {
        System.out.println();
        System.out.println();
        for (int i = 0; i < maatriks.length; i++) {
            tryki(maatriks[i]); // kutsub välja juba kirjutatud meetodi
            System.out.println();
        }
    }
}
