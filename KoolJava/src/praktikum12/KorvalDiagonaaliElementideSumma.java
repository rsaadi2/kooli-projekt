package praktikum12;

/**
 * Created by Kasutaja on 26.01.2016.
 * Arvutada kõrvaldiagonaali elementide summa (kõrvaldiagonaal on see,
 * mis jookseb ülevalt paremast nurgast alla vasakusse nurka).
 * public static int korvalDiagonaaliSumma(int[][] maatriks)
 */
public class KorvalDiagonaaliElementideSumma {
    public static void main(String[] args) {
        int[][] maatriks2D = { { 1, 1, 1, 1, 1 }, { 2, 2, 2, 2, 2 }, { 3, 3, 3, 3, 3 }, { 4, 5, 6, 7, 8 },
                { 5, 6, 7, 8, 9 }, };
        System.out.print(korvalDiagonaaliSumma(maatriks2D));
    }

    public static int korvalDiagonaaliSumma(int[][] maatriks) {
        System.out.println();
        int summa = 0;
        for (int i = 0; i < maatriks.length; i++) {
            for (int j = 0; j < maatriks.length; j++) {
                if (i + j == maatriks.length - 1) {
                    summa = summa + maatriks[i][j];
                }
            }
        }
        return summa;
    }

    /**
     * Arvutada kõrvaldiagonaali elementide summa
     * (kõrvaldiagonaal on see, mis jookseb ülevalt paremast nurgast alla vasakusse nurka).
     *
     * @param maatriks
     * @return
     */
    /*
    public static int korvalDiagonaaliSumma(int[][] maatriks) {
        System.out.println("\n\nÜlesanne 4:");

        int sum = 0;

        for (int i = 0; i < maatriks.length; i++) {
            sum += maatriks[i][maatriks[i].length - i - 1];
        }

        return sum;
    } */
}
