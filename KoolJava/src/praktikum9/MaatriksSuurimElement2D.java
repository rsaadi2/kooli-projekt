package praktikum9;

/**
 * Created by Kasutaja on 26.01.2016.
 * Leida programselt kahemõõtmelise massiivi (a.k.a maatriks, tabel) suurim element. Kasuta selles meetodis ka eelnevalt kirjutatud meetodit, mis leiab ühemõõtmelise massivi maksimaalse elemendi.

 int[][] neo = {
 {1, 3, 6, 7},
 {2, 3, 3, 1},
 {17, 4, 5, 0},
 {-20, 13, 16, 17}
 };
 */
public class MaatriksSuurimElement2D {
    public static void main(String[] args) {

        int[][] neo = { { 1000, 3, 6, 79 },
                { 2, 3, 3333, 1 },
                { 17, 444, 5, 0 },
                { -20, 13, 16, 5 } };
        System.out.println(max(neo));
    }

    public static int suurim(int[] sisend) {

        int seniSuurim = 0;
        for (int i : sisend) {
            if (i > seniSuurim)
                seniSuurim = i;
        }
        return seniSuurim;
    }

    public static int max(int[][] sisend) {

        int maks = 0;

        for (int[] el : sisend) {
            int vaartus = suurim(el);
            if (vaartus > maks) {
                maks = vaartus;
            }
        }
        return maks;
    }
}
