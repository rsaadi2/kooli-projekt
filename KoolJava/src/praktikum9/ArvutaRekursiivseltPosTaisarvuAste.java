package praktikum9;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class ArvutaRekursiivseltPosTaisarvuAste {
    public static void main(String[] args){

        System.out.println(astenda(4,2));
    }

    public static int astenda(int arv, int aste){
        if (aste != 0){
            return (int) Math.pow(arv, aste);
        }else{
            return arv;
        }

    }
}
