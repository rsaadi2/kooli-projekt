package praktikum9;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada tekstirežiimis tulpdiagrammi joonistav programm.

 Küsida kasutajalt järjest numbreid seni, kuni kasutaja sisestab nulli. Negatiivseid arve mitte lubada. Trükkida ekraanile x tähtedest koosnev tulpdiagramm, kus x-e on täpselt sama palju kui oli numbreid.

 Näiteks, kui kasutaja sisestas numbrid 5 3 8 ja 11, oleks diagramm:

 5 xxxxx
 3 xxx
 8 xxxxxxxx
 11 xxxxxxxxxxx
 Joondamise jaoks uuri funktsiooni System.out.format.
 */
public class TulpDiagrammTekstPorgramm {
    public static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args){
        ArrayList<Integer> numbers = new ArrayList<>();
        int curInput = -1;

        //saa kasutajalt numbrid
        while(curInput != 0){
            System.out.print("Sisesta positiivne number: ");
            curInput = getInt();
            //kui number on nullist väiksem:
            if(curInput < 0){
                System.out.print("Proovi uuesti: ");
            }//kui number on null
            else if (curInput == 0){
                System.out.println("Katkestan");
            }//aktsepteeritav number:
            else{
                numbers.add(curInput);
            }
        }

        printHorizontalTable(numbers);

        userInput.close();
    }

    /**
     * prindi tabel horisontaalselt
     * @param mas ArrayList massiiv
     */
    public static void printHorizontalTable(ArrayList<Integer> mas){
        int f = FindMaxValueLength(mas);
        for(int el : mas){
            System.out.format("\n%" + f + "d ", el);
            printX(el);
        }

    }

    /**
     * saa kasutajalt int tyypi arv
     * @return int
     */
    public static int getInt(){
        int input = -1;
        try{
            input = userInput.nextInt();
        }catch(InputMismatchException e){
            userInput.next();
        }
        return input;
    }

    /**
     * Tagasta massiivi suurima elemendi pikkus. mitu numbrikohta on arvus
     * @param massiiv
     * @return int
     */
    public static int FindMaxValueLength(ArrayList<Integer> massiiv){
        int max = Integer.MIN_VALUE;
        for(int el : massiiv){
            if(el > max){
                max = el;
            }
        }
        return String.valueOf(max).length();
    }

    /**
     * prindi antud arv "x" m2rke
     * @param count "x"-ide arv
     */
    public static void printX(int count){
        for(int i = 0; i < count; i++){
            System.out.print("x");
        }
    }
}
