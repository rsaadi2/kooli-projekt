package praktikum9;

/**
 * Created by Kasutaja on 26.01.2016.
 * Leida programselt ühemõõtmelise massiivi suurim element.

 int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3};
 */
public class MassiiviSuurimElement1D {
    public static void main(String[] args) {
        int[] massiiv = {1, 3, 6, 7, 8, 3, 5, 7, 21, 3, 58};
        System.out.println(suurim(massiiv));
    }

    public static int suurim(int[] sisend) {

        int seniSuurim = 0;
        for (int i : sisend) {
            if (i > seniSuurim)
                seniSuurim = i;
        }

        return seniSuurim;
    }
}
