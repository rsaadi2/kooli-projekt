package praktikum9;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Kasutaja on 26.01.2016.
 * Täiendusi taibudele:

 Skaleeri oma diagrammi nii, et kogulaius ei ületa kunagi 80 märki.
 Selleks pead leidma kõige suurema elemendi ja arvutama selle väärtuse järgi kui palju tuleb kõiki elemente jagada, et suurim oleks 80 märki pikk.
 Täienda programmi nõnda (või kirjuta uus), et horisontaalsete tulpade asemel joonistataks vertikaalsed tulbad:
 x
 x
 x
 x  x
 x  x
 x  x
 x     x  x
 x     x  x
 x  x  x  x
 x  x  x  x
 x  x  x  x
 ------------
 5  3  8 11
 */

public class VertikaalDiagramm {
    public static Scanner userInput = new Scanner(System.in);

    public static void main(String[] args){
        ArrayList<Integer> numbers = new ArrayList<>();
        int curInput = -1;

        //saa kasutajalt numbrid
        while(curInput != 0){
            System.out.print("Sisesta positiivne number: ");
            curInput = getInt();
            //kui number on nullist väiksem:
            if(curInput < 0){
                System.out.print("Proovi uuesti: ");
            }//kui number on null
            else if (curInput == 0){
                System.out.println("Katkestan");
            }//aktsepteeritav number:
            else{
                numbers.add(curInput);
            }
        }

        printVerticalTable(numbers);

        userInput.close();
    }

    /**
     * prindi tabel vertikaalselt
     * @param mas
     */
    public static void printVerticalTable(ArrayList<Integer> mas){
        //leian mitu tähemärki on pikk massiivi kőige pikem element
        int f = FindMaxValueLength(mas);
        int maxElement = Integer.MIN_VALUE;
        boolean overTwenty = false;
        double scale = 0;

        //leia, kas mőni element on pikem kui 20 märki
        //kőigepealt leian max elemendi
        for(int el : mas){
            if(el > maxElement){
                maxElement = el;
            }
        }
        // kui mőni arv oli suurem kui 20
        if(maxElement > 20){
            overTwenty = true;
            //leian väärtuse millega tuleb vastavat arvu läbi korrutada
            scale = (double) 20 / (double) maxElement;
        }
        // leian prinditava tabeli kőrguse
        int tableHeight;
        if(overTwenty){
            // kui suurim arv on üle 20, leian kordaja millega arve korrutada, et tabel oleks max 20 märki kőrge
            tableHeight = (int) (Math.round(scale * maxElement));
        }else{
            //kui suurim arv on alla 20-ne, on tabeli kőrguseks see suurim arv
            tableHeight = maxElement;
        }
        //iga rea kohta
        for(int i = tableHeight - 1; i >= 0; i--){
            //iga elemendi kohta:
            for (int j = 0; j < mas.size(); j++){
                //kui tabel alla 20 märgi ja j element on suurem kui i väärtus, tuleb printida x VŐI
                //kui tabel alla 20 märgi ja j element korrutatuna kordajaga  on suurem kui i väärtus, tuleb printida x
                if((!overTwenty && mas.get(j) > i) || (overTwenty && (scale * mas.get(j)) > i)){
                    System.out.printf("%" + (f + 1) + "s", "x");
                }else{ // muidu prindi tühikud
                    System.out.printf("%" + (f + 1) + "s", "");
                }
            }
            //reavahetus
            System.out.println();
        }
        //prindi alumised 2 rida
        int markCount = mas.size() * (f + 1);
        for(int i = 0; i < markCount; i++){
            System.out.print("-");
        }
        System.out.println();
        for(int el : mas){
            System.out.printf("%"+ (f + 1) + "d", el);
        }


    }


    /**
     * prindi tabel horisontaalselt
     * @param mas ArrayList massiiv
     */
    public static void printHorizontalTable(ArrayList<Integer> mas){
        //leian mitu tähemärki on pikk massiivi kőige pikem element
        int f = FindMaxValueLength(mas);
        //leia, kas mőni element on pikem kui 80 märki
        //kőigepealt leian max elemendi
        int maxElement = Integer.MIN_VALUE;
        boolean overEighty = false;
        double scale = 0;

        for(int el : mas){
            if(el > maxElement){
                maxElement = el;
            }
        }
        if(maxElement > 80){
            overEighty = true;
            //leian väärtuse millega tuleb vastavat arvu läbi korrutada
            scale = (double) 80 / (double) maxElement;
        }

        //prindi iga elemendi kohta välja vastav rida
        for(int el : mas){

            System.out.format("\n%" + f + "d ", el);

            //kui on vaja ridade pikkusi muuta
            if(overEighty){
                System.out.print(" " + scale + " ");
                //leian mitu x-i tuleb vastava arvu puhul printida
                int numOfXes = (int) (Math.round(scale * el));
                printX(numOfXes);
            }else { //kui pole vaja

                printX(el);
            }

        }//endOf for

    }//endOf Method

    /**
     * saa kasutajalt int tyypi arv
     * @return int
     */
    public static int getInt(){
        int input = -1;
        try{
            input = userInput.nextInt();
        }catch(InputMismatchException e){
            userInput.next();
        }
        return input;
    }

    /**
     * Tagasta massiivi suurima elemendi pikkus. mitu numbrikohta on arvus
     * @param massiiv
     * @return int
     */
    public static int FindMaxValueLength(ArrayList<Integer> massiiv){
        int max = Integer.MIN_VALUE;
        for(int el : massiiv){
            if(el > max){
                max = el;
            }
        }
        return String.valueOf(max).length();
    }

    /**
     * prindi antud arv "x" m2rke
     * @param count "x"-ide arv
     */
    public static void printX(int count){
        for(int i = 0; i < count; i++){
            System.out.print("x");
        }
        System.out.print(" " + count);
    }

}
