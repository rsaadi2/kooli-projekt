package praktikum4;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ühele reale paarisarvud 0 kuni 10 (0,2,4 jne.)
 */
public class ForNullistKahekymneNeljani {
    public static void main(String[] args) {
        // 1.variant
        /*
        for (int i = 0; i <= 24; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            } */

        System.out.println();
        // 2.variant
        for (int j = 0; j <= 24; j += 2) {
            System.out.print(j + " ");
        }
    }
}

