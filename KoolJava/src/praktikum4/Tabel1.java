package praktikum4;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ekraanile selline tabel:
 1 0 0 0 0 0 0
 0 1 0 0 0 0 0
 0 0 1 0 0 0 0
 0 0 0 1 0 0 0
 0 0 0 0 1 0 0
 0 0 0 0 0 1 0
 0 0 0 0 0 0 1
 Tabeli suurus olgu lihtsasti muudetav.
 */
public class Tabel1 {
    public static void main(String[] args) {
        int suurus = 7;

        for (int x = 0; x < suurus; x++){
            for (int y = 0; y < suurus; y++){
                if (x == y) {
                    System.out.print("1 ");
                } else {
                    System.out.print("0 ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
/*
int tabeliSuurus = 7;

		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j) {
					System.out.print("1 ");
				} else {
					System.out.print("0 ");
				}
			}
			System.out.println(); // reavahetus
		}
		System.out.println();
*/

/*
final int x_size = 7;  // table size in x direction
		final int y_size = 7;  // table size in y direction

		// Create the table
		for (int x = 0; x <= x_size; x++) {
			for (int y = 0; y <= y_size; y++) {
				if (x == y) {
					System.out.printf("1 ");
				} else {
					System.out.printf("0 ");
				}
			}
			//we are at the end of the line, start a new one
			System.out.printf("%n");
 */