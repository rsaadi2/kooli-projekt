package praktikum4;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ühele reale kolmega jaguvad arvud vahemikus 30 kuni 0 (30, 27, 24 jne.)
 */
public class ForJaguvadKolmega {
    public static void main(String[] args) {
        // 1.meetod
        for (int i = 30; i >= 0; i -= 3) {
            System.out.print(i + " ");
        }
        System.out.println();
        // 2.meetod
        for (int i = 30; i >= 0; i--) {
            if (i % 3 == 0) {
                System.out.print(i + " ");
            }
        }
    }
}
