package praktikum4;

import lib.TextIO;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ekraanile selline tabel koos ääristega. Tabeli kõrgus on sama mis laius -
 * see suurus küsi kasutaja käest.
 -----------------
 | x 0 0 0 0 0 x |
 | 0 x 0 0 0 x 0 |
 | 0 0 x 0 x 0 0 |
 | 0 0 0 x 0 0 0 |
 | 0 0 x 0 x 0 0 |
 | 0 x 0 0 0 x 0 |
 | x 0 0 0 0 0 x |
 -----------------
 */
public class Tabel2 {
    public static void main(String[] args) {
        int laius;
        System.out.println("mis on tabeli laius");
        laius = TextIO.getInt();

        for (int i = 0; i < laius * 2 + 3; i++){
            System.out.print("-");
        }
        System.out.println();
        for (int i = 0; i < laius; i++) {
            System.out.print("| ");
            for (int j = 0; j < laius; j++) {
                if (i == j || i + j == laius - 1) {
                    System.out.print("x ");
                } else {
                    System.out.print("0 ");
                }

            }
            System.out.println("|"); // reavahetus
        }
        for (int i = 0; i < laius * 2 + 3; i++){
            System.out.print("-");
        }

    }
}

/*
int tabeliSuurus = 7;

		for (int i = 0; i < tabeliSuurus * 2 + 3; i++){
		System.out.print("-");
		}
		System.out.println();
		for (int i = 0; i < tabeliSuurus; i++) {
			System.out.print("| ");
			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j || i + j == tabeliSuurus - 1) {
					System.out.print("x ");
				} else {
					System.out.print("0 ");
								}

			}
			System.out.println("|"); // reavahetus
		}
		for (int i = 0; i < tabeliSuurus * 2 + 3; i++){
			System.out.print("-");
			}
 */