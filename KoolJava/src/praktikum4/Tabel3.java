package praktikum4;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ekraanile selline tabel:
 0 1 2 3 4 5 6 7 8 9
 1 2 3 4 5 6 7 8 9 0
 2 3 4 5 6 7 8 9 0 1
 3 4 5 6 7 8 9 0 1 2
 4 5 6 7 8 9 0 1 2 3
 5 6 7 8 9 0 1 2 3 4
 6 7 8 9 0 1 2 3 4 5
 7 8 9 0 1 2 3 4 5 6
 8 9 0 1 2 3 4 5 6 7
 9 0 1 2 3 4 5 6 7 8
 */
public class Tabel3 {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {

            for (int j = 0; j < 10; j++) {

                if (i + j < 10){
                    System.out.print(j + i + " ");
                } else {
                    System.out.print(j + i - 10 + " ");
                }
            }
            System.out.println();
        }



    }

}
