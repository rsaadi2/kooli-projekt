package praktikum4;

/**
 * Created by Kasutaja on 20.01.2016.
 * Trükkida ühele reale numbrid 10 kuni 1
 */
public class ForYhestKymneni {
    public static void main(String[] args) {
        // 1.variant
        for (int i = 10; i > 0; i--) {
            System.out.print(i + " ");
        }
        System.out.println();
        //2.variant
        for (int u = 1; u <= 10; u++) {
            System.out.print(u + " ");
        }
    }
}
