package praktikum4;

/**
 * Created by Kasutaja on 22.01.2016.
 */
public class ArraydKatse2 {
    public static void main(String[] args) {

    }

    static void printDivisors( int N ) {
        int D; // One of the possible divisors /jagaja of N.
        System.out.println("The divisors of " + N + " are:");
        for ( D = 1; D <= N; D++ ) {
            if ( N % D == 0 )  // Dose D evenly divide N?
                System.out.println(D);
        }
    }
}
