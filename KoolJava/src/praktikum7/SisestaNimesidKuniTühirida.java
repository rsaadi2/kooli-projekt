package praktikum7;

import lib.TextIO;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis laseb kasutajal sisestada senikaua nimesid,
 * kui kasutaja sisestab nime asemel tühja rea. Seejärel trükkida nimed
 * ekraanile tähestikulises järjekorras. Vihje: selle ülesande kasutamiseks
 * kasuta ArrayList klassi. Sorteerimiseks kasuta Collections.sort() meetodit.
 * ArrayList<String> nimed = new ArrayList<String>();
 * nimed.add("Mati");
 * for (String nimi : nimed) {
 * System.out.println(nimi);
 * }
 */
public class SisestaNimesidKuniTühirida {
    public static void main(String[] args) {

        ArrayList<String> nimed = new ArrayList<String>();

        while (true) {
            System.out.print("Kirjuta nimi: ");
            String nimi = lib.TextIO.getlnString();
            if (nimi.isEmpty()) {
                break;
            }
            nimed.add(nimi);
        }

        // sort
        Collections.sort(nimed, null);

        System.out.println("Sisestatud nimed on: ");
        for (String nimi : nimed) {
            System.out.println(nimi);
        }

    }
}

        /*ArrayList<String> nimed = new ArrayList<String>();
        String nimi;
        do {
            System.out.println("Palun sisesta nimed, lőpetamiseks sisesta tühi rida.");
            nimi = TextIO.getln();

            nimed.add(nimi);
        } while (nimi.length() != 0);

        for (String n : nimed) {
            System.out.println(n);
        }*/


