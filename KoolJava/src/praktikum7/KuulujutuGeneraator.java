package praktikum7;

import java.util.Random;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada kuulujutugeneraator.
 Kolm ühepikkust massiivi: Esimeses naisenimed, teises mehenimed, kolmandas tegusõnad.
 Programm võtab igast massiivist ÜHE suvalise elemendi ja kombineerib nendest lause.
 Vihje: Massivist suvalise elemendi valimiseks arvuta random number vahemikus 0 kuni
 massiivi pikkus (random * length).
 */
public class KuulujutuGeneraator {
    public static void main(String[] args) {

        String[] naised = { "Juuli", "Maali", "Tuuli" };
        String[] mehed = { "Lembit", "Ants", "Vambola" };
        String[] tegu = { "hüüdis", "kutsus", "kiljus" };

        System.out.println(suvalineElement(naised) + " " + suvalineElement(tegu) + " " + suvalineElement(mehed));
    }

    public static String suvalineElement(String[] massiiv) {
        int jrk = new Random().nextInt(massiiv.length);
        return massiiv[jrk];
    }
}
