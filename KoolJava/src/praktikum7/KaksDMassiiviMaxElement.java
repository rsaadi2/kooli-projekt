package praktikum7;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada meetod, mis leiab ja tagastab ettantud kahemõõtmelise massiivi maksimaalse elemendi.
 * Kirjuta kõigepealt meetod, mis leiab ühemõõtmelise massiivi maksimaalse elemendi ning
 * kasuta siis seda meetodit teises:
 // meetod, mis leiab ühemõõtmelise massiivi maksimumi
 public static int maksimum(int[] massiiv)

 // meetod, mis leiab maatriksi maksimumi
 publiv static int maksimum(int[][] maatriks) {
 // maatriksi rea maksimumi leidmiseks saad siin edukalt kasutada eelmist meetodit
 }
 */
public class KaksDMassiiviMaxElement {
    public static void main(String[] args) {

        System.out.println(maksimum(new int[][] { { 11, 2, 3, 45 }, { 4, 7, 3 }, { 4, 3, 1 } }));
    }

    // meetod, mis leiab ühemőőtmelise massiivi maksimumi
    public static int maksimum(int[] massiiv) {
        int max = 0;

        for (int i = 0; i < massiiv.length; i++) {
            if (massiiv[i] > max) {
                max = massiiv[i];
            }
        }
        return max;
    }

    // meetod, mis leiab maatriksi maksimumi
    public static int maksimum(int[][] maatriks) {
        int max = 0;
        for (int i = 0; i < maatriks.length; i++) {
            for (int j = 0; j < maatriks[i].length; j++) {
                if (maksimum(maatriks[i]) > max) {
                    max = maksimum(maatriks[i]);
                }
            }
        }
        return max;
    }
}
