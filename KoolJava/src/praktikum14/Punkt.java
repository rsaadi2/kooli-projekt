package praktikum14;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class Punkt {


    // extends Object vaikimisi olemas

    int x;
    int y;

    public Punkt(int x, int y) {
        this.x = x; // kui oleks i ja j, siis x=i ja y=j
        this.y = y;

        // konstruktormeetod on see meetod,
        // mis kutsutakse välja, kui me loome uue objekti
        // konstruktoreid vőib olla koodis mitu tükki, neid
        // valitakse välja argumendi väärtuse järgi
    }

    public Punkt() {
        // Luuakse punkt, millel ei ole koordinaate mĂääratud
    }
    @Override // annotatsioon, ei muuda programmi tööd,
    // aga need on vihjed Eclipsile,
    // kuidas me oleme programmi üles ehitanud
    // ja mida me tahame kindlasti teha
    public String toString() {
        // TODO trükkida välja punkt

        return "Punkt(x=" + x + " ,y=" + y + ")";
    }


}
