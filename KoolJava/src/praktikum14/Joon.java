package praktikum14;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class Joon {
    Punkt algPunkt;
    Punkt loppPunkt;

    public Joon(Punkt x, Punkt y) {
        algPunkt = x;
        loppPunkt = y;

    }

    @Override
    public String toString() {

        return "Joon(" + algPunkt + "kuni " + loppPunkt + ")";
    }

    public double pikkus() {
        double a = loppPunkt.x - algPunkt.x;
        double b = loppPunkt.y - algPunkt.y;
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));

    }
}
