package praktikum14;
/*
/**
 * Created by Kasutaja on 26.01.2016.
 */
/*
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;
import praktikum11.Ring;

import java.util.ArrayList;
import java.util.List;



public class Joonistamine extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("JavaFX-iga joonistamise n2ide");
        Group root = new Group();
        Canvas canvas = new Canvas(400, 400);
        GraphicsContext gc = canvas.getGraphicsContext2D();

        //Ring
        //joonistaRing(gc, Color.GREEN, new Ring(100,100, 30));
        //JOON
        joonistaJoon(gc, Color.RED, new Joon(new Punkt(10, 10), new Punkt(200, 120)));
        //Punkt
        joonistaPunkt(gc, Color.BLUE, new Punkt(180, 120));

        //PunktiMassiiv
        List<Punkt> punktid = getPoints(10);
        joonistaPunktiKogum(punktid, gc, Color.AQUA);


        root.getChildren().add(canvas);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    private void joonistaPunktiKogum(List<Punkt> punktid, GraphicsContext gc, Paint p){
        gc.setFill(p);
        for (int i = 1; i < punktid.size(); i++){
            gc.strokeLine(punktid.get(i - 1).getxKoordinaat(), punktid.get(i - 1).getyKoordinaat(),
                    punktid.get(i).getxKoordinaat(), punktid.get(i).getyKoordinaat());
        }
    }

    private List<Punkt> getPoints(int count){
        List<Punkt> punktid = new ArrayList<>();
        for (int i = 0; i < 10; i++){
            punktid.add(new Punkt(Math.random() * 400, Math.random() * 400));
        }
        return punktid;
    }

    private void joonistaRing(GraphicsContext gc, Paint p,  Ring ring){
        gc.setFill(p);
        gc.fillOval(ring.getxKoordinaat(), ring.getyKoordinaat(), ring.getRaadius() * 2, ring.getRaadius() * 2);
    }

    private void joonistaJoon(GraphicsContext gc, Paint p, Joon joon){
        gc.setFill(p);
        gc.strokeLine(joon.getAlgus().getxKoordinaat(), joon.getAlgus().getyKoordinaat(),
                joon.getL6pp().getxKoordinaat(), joon.getL6pp().getyKoordinaat());
    }

    private void joonistaPunkt(GraphicsContext gc, Paint p, Punkt punkt){
        gc.setFill(p);
        gc.fillOval(punkt.getxKoordinaat(), punkt.getyKoordinaat(), 3, 3);
    }


}*/
