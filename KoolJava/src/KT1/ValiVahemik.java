package KT1;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjuta funktsioon valiVahemik, mis võtab ühe int tüüpi argumendi ja tagastab:
 * Arvu 0 kui argument oli väiksem kui 10
 * Arvu 1 kui argument oli vahemikus 10 - 35
 * Arvu 2 kui argument oli suurem kui 35
 * <p>
 * public static int valiVahemik (int m)
 */
public class ValiVahemik {
    //public class Answer {

    public static void main(String[] args) {
        System.out.println(valiVahemik(0));
        System.out.println(valiVahemik(-25));
    }

    public static int valiVahemik(int m) {
        if (m < 10) {
            return 0;
        } else if (m >= 10 && m <= 35) {
            return 1;
        } else {
            return 2;
        }
    }
}

