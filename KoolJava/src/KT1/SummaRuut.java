package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java-meetod, mis leiab kahe etteantud reaalarvu summa ruudu.
 * Write a method in Java to find the square of the sum of two given real numbers.
 */
public class SummaRuut {
//    public class Answer {

    public static void main(String[] args) {
        System.out.println(summaRuut(5., -5.));
        System.out.println(summaRuut(2., -7.));
        System.out.println(summaRuut(9., 15.));
        // YOUR TESTS HERE
    }

    public static double summaRuut(double a, double b) {

        double summaRuut = Math.pow((a + b), 2);

        return summaRuut;
    }

}


