package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjuta funktsioon numberOfItems, mis tagastab esimese argumendina
 * ette antud int tüüpi massiivi pikkuse.
 * public static int numberOfItems(int [] m)
 */
public class NumberOfItems {
    //   public class Answer {
    public static int numberOfItems(int[] m) {
        return m.length;
               // return m[0]; //Esimene element massiivis
    }

    public static void main(String[] args) {
        int[] test = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

        System.out.println(numberOfItems(test));
    }
}