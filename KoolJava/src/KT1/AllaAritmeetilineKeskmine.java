package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu,
 * mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest
 * (aritmeetiline keskmine = summa / elementide_arv).
 * public static int allaKeskmise (double[] d)
 */
public class AllaAritmeetilineKeskmine {
    //public class Answer {

    public static void main(String[] args) {
//        System.out.println(allaKeskmise(new double[]{0.}));
        System.out.println(allaKeskmise(new double[]{5, 9, 0, 1}));
        // YOUR TESTS HERE
       
    }

    public static int allaKeskmise(double[] d) {
        double keskmine = 0;
        int elementideHulk = 0;
        //get average
        for (int i = 0; i < d.length; i++) { //d.lenght on alati 4, ehk nagu ma alguses aru sain
            System.out.println(keskmine += d[i]);//d[i] inda elemendi väärtus. seega keskmine siis
            // on lihtsalt elementide summa. viimaseks väärtuseks on 15, kuna 5 + 9 + 1 on 15. 
        }
        keskmine = keskmine / d.length;
        //get count
        for (int i = 0; i < d.length; i++) {
            if (d[i] < keskmine) {
                elementideHulk++;
            }
        }
        //System.out.println(keskmine);
        return elementideHulk;
    }
  
}



