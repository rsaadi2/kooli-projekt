package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java-meetod, mis leiab etteantud massiivi m negatiivsete elementide summa.
 * public static int negSumma (int[] m)
 */
public class NegElementideArvSumma {
    // public class Answer {

    public static void main(String[] args) {
        System.out.println(negSumma(new int[]{0}));
        System.out.println(negElArv(new int[]{-5, 6, -7, 9, -2}));
        System.out.println(negSumma(new int[]{-5, 6, -7, 9, -2}));
    }

    public static int negElArv(int[] m) { //annab negatiivsete elementide arvu
        int n = 0;
        for (int i = 0; i < m.length; i++)
            if (m[i] < 0)
                n++;
        return n;
    }

    public static int negSumma(int[] m) { //annab negatiivsete elementide summa
        int s = 0;
        for (int i = 0; i < m.length; i++)
            if (m[i] < 0)
                s += m[i];
        return s;
    }
}



