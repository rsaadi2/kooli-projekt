package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class PosPaaris {
    public static void main(String[] args) {

        System.out.println(posPaarisarv(4));
    }

    public static boolean posPaarisarv(int n){
        if (n > 0 && n % 2 == 0){
            return true;
        }
        else{
            return false;
        }
    }
}
