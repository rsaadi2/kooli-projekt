package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class ArvutaSumma {
    //public class Answer {
    public static int arvutaSumma(int a, int b) {
        int summa = a + b;
        return summa;
    }

    public static void main(String[] args) {
        System.out.println(arvutaSumma(5, 10));
        System.out.println(arvutaSumma(-5, 89));
        System.out.println(arvutaSumma(-60, -10));
    }
}

