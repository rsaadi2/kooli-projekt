package KT1;

/**
 * Created by Kasutaja on 25.01.2016.
 * Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal
 * niisuguste elementide arvu, mis on rangelt suuremad kõigi elementide
 * aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv).

 public static int keskmisestParemaid (double[] d)
 */
public class SuuremAritmeetilineKeskmine {
    //public class Answer {

    public static void main(String[] args) {
        System.out.println (keskmisestParemaid (new double[]{0.}));
        System.out.println(keskmisestParemaid(new double[]{5, 9, 0, 4}));
           }

    public static int keskmisestParemaid(double[] d) {

        double summa = 0;
        for (int i = 0; i < d.length; i++) {
            summa = summa + d[i];
        }
        double keskmine = 0;
        keskmine = summa / d.length;
        int suurem = 0;

        for (int i = 0; i < d.length; i++) {
            if (d[i] > keskmine) {
                suurem = 1 + suurem;
            }
        }
        return suurem;
    }
}

 /* return -1;
       // double x = 0;
        double y = 0;
        for (int i = 0; i < d.length; i++)
            y = y + d[i];
        double x = y / d.length;
        int k = 0;
        for (int i = 0; i < d.length; i++)
            if (d[i] > x)
                k++;
        return (k);
    }
*/