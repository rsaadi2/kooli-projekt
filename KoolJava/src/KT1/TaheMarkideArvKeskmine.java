package KT1;

/**
 * Created by Kasutaja on 25.01.2016.
 * Kirjuta programm, mis leiab etteantud sõnademassiivi sõnade
 * tähemärkide arvu aritmeetilise keskmise.
 * public static double averageLength(String[] words)
 */
public class TaheMarkideArvKeskmine {

    public static void main(String[] args) {
        // should print out 3.0
        System.out.println(averageLength(new String[]{"this", "is", "fun"}));
    }

    public static double averageLength(String[] words) {
        int[] averageArray = new int[words.length];
        for (int i = 0; i < words.length; i++)
            averageArray[i] = words[i].length();
        int sum = 0;
        for (int el : averageArray)
            sum += el;
        return sum / words.length;
    }
}


