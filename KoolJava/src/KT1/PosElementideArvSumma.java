package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class PosElementideArvSumma {

    // public class Answer {

    public static void main(String[] args) {
        System.out.println(posSumma(new int[]{0}));
        System.out.println(poselArv(new int[]{-5, 6, -7, 9, -2}));
        System.out.println(posSumma(new int[]{-5, 6, -7, 9, -2}));
    }

    public static int poselArv(int[] m) { //annab positiivsete liikmete arvu
        int posElemendid = 0;
        for (int i = 0; i < m.length; i++) {
            if (m[i] > 0) {
                posElemendid++;
            } else continue;
        }
        return posElemendid;
    }
    public static int posSumma(int[] m) { //annab positiivsete elementide summa
        int s = 0;
        for (int i = 0; i < m.length; i++)
            if (m[i] > 0)
                s += m[i];
        return s;
    }
}
