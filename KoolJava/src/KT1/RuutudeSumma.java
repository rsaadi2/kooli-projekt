package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 *
 */
public class RuutudeSumma {
    public static void main (String[] args) {

        System.out.println(ruutudeSumma(new int[]{1, 3, 0, 5, 0, 6, 0}));
    }

    // public staatiline meetod, mis tagastab int tüübi ja saab sisendiks int massiivi m .
    //nimi "m" kasutatav vaid meetodi sees
    public static int ruutudeSumma(int[] m){
        //summa loendur, mille algväärtus on null.
        int summa = 0;
        //loe: iga massiivi "m" nn sahtli, mille nimeks panen tsükli sees kasutuseks "element", tee seda:
        for(int element : m){
            //liida olemasolevale summale juurde elemendi teise astme väärtus.
            // Math.pov(<astendatav>, <astendaja>
            summa += Math.pow(element, 2);
        }
        //tagastan lõpliku summa pärast kõigi elemendide ruutude liitmist.
        return summa;
    }
}
