package KT1;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java-meetod, mis leiab kahe etteantud reaalarvu summa absoluutväärtuse.

 NB! teine versioon küsib summa absväärtust!
 public static double summaAbsv (double a, double b)
 */
public class SummaAbsv {
    //public class Answer {

    public static void main(String[] args) {

        System.out.println(summaAbsv(0., 0.));
        System.out.println(summaAbsv(1, -2));
        System.out.println(summaAbsv(-7, -2));

    }

    public static double summaAbsv(double a, double b) {
        double summa = a + b;
        return Math.abs(summa);
    }
}
