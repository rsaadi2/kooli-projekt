package KT1;

/**
 * Created by Kasutaja on 25.01.2016.
 * Koostage Java-meetod, mis leiab kahe etteantud reaalarvu absoluutväärtuste summa.

  NB! teine versioon küsib summa absväärtust!
 public static double summaAbsv (double a, double b)
 */
public class AbsSumma {
//public class Answer {

    public static void main (String[] args) {
        System.out.println (absvdeSumma (0., 0.));
        System.out.println(absvdeSumma (9.4 ,-18.2));
    }
        public static double absvdeSumma (double a, double b) {
        double summa = Math.abs(a) + Math.abs(b);
       // return Math.abs(a) + Math.abs(b);
        return summa;
    }
}

