package praktikum3;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada "tehisintellekt". Programm küsib kasutajalt kaks vanust.
 Kui vanuste vahe on rohkem kui viis aastat ütleb midagi krõbedat.
 Kui vanuste vahe on rohkem kui kümme aastat ütleb midagi veel krõbedamat.
 Kui vanuste vahe jääb alla viie aasta, ütleb, et sobib.
 NB! Programmi töö ei tohi sõltuda vanuste sisestamise järjekorrast!
 */
public class VanuseVahe {
    public static void main(String[] args) {
        int vanus1, vanus2, arvuta;
        System.out.println("sisesta esimese inimese vanus");
        vanus1 = TextIO.getInt();
        System.out.println("sisesta teise inimese vanus");
        vanus2 = TextIO.getInt();

        arvuta = Math.abs(vanus1-vanus2);
        System.out.println("vanusevahe on " + arvuta);

        if (arvuta <= 5){
            System.out.println("sobib");
        } else if (arvuta <= 10) {
            System.out.println("pole viga");
        } else if (arvuta > 5) {
            System.out.println("hullumaja");
        } else {
            System.out.println("pole võimalik");
        }

    }
}
