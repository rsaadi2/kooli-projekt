package praktikum3;

import lib.TextIO;

/**
 * Created by Kasutaja on 20.01.2016.
 * Kirjutada programm, mis küsib kasutajalt parooli ning annab teada,
 * kas kasutaja sisestas õige või vale parooli (õige parool olgu programmis kirjas).
 * Pane tähele, et String-tüüpi väärtusi ei saa Javas edukalt võrrelda kahe
 * võrdusmärgiga, vaid tuleb kasutada equals-meetodit.
 */
public class OigeParool {
    public static void main(String[] args) {
        int siseArv = 0;

        while (true) {
            String parool = "nipitiri";
            String sisestus = "";

            System.out.println("sisesta parool");
            sisestus = TextIO.getln();
            if (parool.equals(sisestus)) {
                System.out.println("parool õige");
                break;
            } else if (!parool.equals(sisestus)) {
                System.out.println("vale parool");
                siseArv = siseArv + 1;
            }

            if (siseArv >= 3) {
                System.out.println("kõik valed");
                break;
            }
        }
    }
}
/*
String oigeParool = "parool";
		String sisestatudParool="";

		while(!oigeParool.equals(sisestatudParool)){
			if(sisestatudParool.equals("")){
				System.out.println("Sisesta parool");
			}
			else{
				System.out.println("Sisesta 6IGE parool");
			}
			sisestatudParool=TextIO.getlnString();

			if(oigeParool.equals(sisestatudParool)){
				System.out.println("Parool 6ige");
			}
			else{
				System.out.println("parool vale");
			}
		}
 */


/*
int sisestusteArv = 0;
		while (true) {
			String parool = "saladus";
			System.out.println("Sisesta parool");
			String kasutajaSisestus = TextIO.getlnString();

			if (parool.equals(kasutajaSisestus)) {
				System.out.println("Õige parool");
				break;
			} else {
				System.out.println("Vale parool");
				sisestusteArv = sisestusteArv + 1;
			}

			if (sisestusteArv >= 3) {
				System.out.println("Sisestasid kolm korda vale parooli, aitab küll!");
				break;
			}
		}
 */