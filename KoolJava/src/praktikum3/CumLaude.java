package praktikum3;

import lib.TextIO;

/**
 * Created by Kasutaja on 19.01.2016.
 * Kirjutada Cum-Laude detektor.
 Programm küsib kasutajalt kaalutud keskhinde ja lõputöö hinde.
 Kui keskhinne on suurem kui 4.5 ja lõputöö hinne on 5 siis ütleb
 "Jah saad cum laude diplomile!". Muul juhul ütleb "Ei saa!"
 NB! Programm ei tohi aktsepteerida vigaseid hindeid (üle 5-e,
 negatiivseid jms) Vigase hinde puhul tuleb anda veateade.
 */
public class CumLaude {
    public static void main(String[] args) {
        double kesk, lopp;
        System.out.println("anna keskmine hinne");
        kesk = TextIO.getDouble();
        System.out.println("anna lõputöö hinne");
        lopp = TextIO.getDouble();

        if (kesk >= 4.5 && lopp == 5){
            System.out.println("saad cum laude");
        } else {
            System.out.println("ära unista");
        }

    }
}
