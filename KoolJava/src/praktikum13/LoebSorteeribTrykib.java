package praktikum13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class LoebSorteeribTrykib {

    public static ArrayList<String> loeFail(String failinimi) {

        // tahame lugeda ükskőik millist faili

        File file = new File(failinimi);
        ArrayList<String> read = new ArrayList<String>();


        try {
            // avame faili lugemise jaoks
            @SuppressWarnings("resource")
            BufferedReader in = new BufferedReader(new FileReader(file));
            String rida;

            // loeme failist rida haaval
            while ((rida = in.readLine()) != null) {
                read.add(rida);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Faili ei leitud: \n" + e.getMessage());
        } catch (Exception e) {
            System.out.println("Error, jee, mingi muu error: " + e.getMessage());
        }
        return read;
    }

    public static void main(String[] args) {

        // punkt tühistab jooksvat kataloogi
        String kataloogitee = LoebSorteeribTrykib.class.getResource(".").getPath();
        ArrayList<String> failiSisu = loeFail(kataloogitee + "kala.txt");
        System.out.println(failiSisu);
        Collections.sort(failiSisu);
        System.out.println(failiSisu);
    }
}

