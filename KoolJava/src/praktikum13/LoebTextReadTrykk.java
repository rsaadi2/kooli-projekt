package praktikum13;

import java.util.ArrayList;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * Created by Kasutaja on 26.01.2016.
 * Kirjutada programm, mis loeb tekstifailist maatriksi read
 * ning trükib välja selle transponeerimise tulemuse.
 */
public class LoebTextReadTrykk {

    public static void main(String[] args) {

        ArrayList<String[]> maatrix = new ArrayList<>();

        String kataloogitee = LoebTextReadTrykk.class.getResource(".").getPath();
        File file = new File(kataloogitee + "maatriks.txt");

        try {
            @SuppressWarnings("resource")
            BufferedReader in = new BufferedReader(new FileReader(file));
            String rida;

            while ((rida = in.readLine()) != null) {
                try {
                    maatrix.add(rida.split(" "));

                } catch (Exception e) {
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        double[][] matrix = arrayGen(maatrix);
        printMatrix(transpose(matrix));
    }

    public static double[][] arrayGen(ArrayList<String[]> list) {
        //leian suurima rea pikkuse uue maatriksi mõõtmeteks
        int maxCol = Integer.MIN_VALUE;
        for (String[] el : list) {
            if (el.length > maxCol) {
                maxCol = el.length;
            }
        }
        //uus maatriks
        double[][] matrix = new double[list.size()][maxCol];
        //täidan maatriksi
        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < list.get(i).length; j++) {
                try {
                    matrix[i][j] = Double.parseDouble(list.get(i)[j]);
                } catch (Exception e) {
                }
            }
        }
        return matrix;
    }

    public static double[][] transpose(double[][] matrix) {
        int rowSize = matrix.length;
        int columnSize = matrix[0].length;
        double[][] transpose = new double[columnSize][rowSize];

        for (int row = 0; row < rowSize; row++) {
            for (int col = 0; col < columnSize; col++) {
                transpose[col][row] = matrix[row][col];
            }
        }
        return transpose;
    }

    public static void printMatrix(double[][] matrix) {
        for (double[] row : matrix) {
            for (double col : row) {
                System.out.printf("%.0f ", col);
            }
            System.out.println();
        }
    }

}

