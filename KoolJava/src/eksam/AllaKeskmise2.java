package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class AllaKeskmise2 {
    public static void main (String[] args) {
        System.out.println (allaKeskmise (new double[]{0., 1., 2., 3., 4.}));
        // YOUR TESTS HERE
    }

    public static int allaKeskmise (double[] d) {
        double summa = 0;
        double keskmine = 0;
        int count = 0;

        for (int i = 0; i < d.length; i++) {
            summa = summa + d[i];//**summa peab tulema 10, aga ma ei saa aru, kuidas saadakse 10.
            // miks on d[i] 10 ?
        }
        keskmine = summa / d.length;// see juba lihtne, summa 10 ja lenght on 5, kuna viis numbrit
        // ja keskmine tuleb 2

        for (int i = 0; i < d.length; i++) {
            if (d[i] < keskmine){// ja see ka selge, kui number alla keskmise, siis lisab ühe juurde. 
                count = count + 1;
            }
        }
        return count;  // YOUR PROGRAM HERE
    }

}
