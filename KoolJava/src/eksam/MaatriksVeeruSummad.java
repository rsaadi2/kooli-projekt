package eksam;

import java.util.Arrays;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class MaatriksVeeruSummad {
   // public class Answer {

    public static void main(String[] args) {

        int[] res = veeruSummad(new int[][]{{1, 2, 3}, {4, 5, 6}});
        System.out.println(Arrays.toString(res));
    }

    public static int[] veeruSummad(int[][] m) {

        int rida = 0;
        int veerg = 0;
        int max = 0;

        for (int i = 0; i < m.length - 1; i++) {
            if (i == 0) {
                if (m[i].length >= m[i + 1].length) {
                    max = m[i].length;
                } else {
                    max = m[i + 1].length;
                }
            }
            if (i > 0) {
                if (m[i + 1].length > max) {
                    max = m[i + 1].length;
                }
            }
        }

        int[] summa = new int[max];


        for (rida = 0; rida < m.length; rida++) {

            for (veerg = 0; veerg < m[rida].length; veerg++) {

                summa[veerg] = summa[veerg] + m[rida][veerg];
            }
        }

        return summa;
    }

}
