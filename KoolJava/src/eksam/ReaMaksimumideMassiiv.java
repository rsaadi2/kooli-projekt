package eksam;

import java.util.Arrays;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class ReaMaksimumideMassiiv {
    //public class Answer {

    /* Koostage Java meetod etteantud täisarvumaatriksi m reamaksimumide massiivi
	 * leidmiseks (massiivi i-s element on maatriksi i-nda rea suurima elemendi väärtus).
	 * Read võivad olla erineva pikkusega.
	   */

    public static void main (String[] args) {

        //1. variant
        int[] res = reaMaxid (new int[][] { {1,2,3}, {4,5,6} });
        System.out.println(Arrays.toString(res));// {3, 6}

       // 2. variant
        int[] res2 = reaMaxid2 (new int[][] { {4,5,6}, {7,8,9} });
        System.out.println(Arrays.toString(res2)); // [6, 9]

    }

    //1. variant
    public static int[] reaMaxid (int[][] m) {

        int[] max = new int[m.length];

        for (int i = 0; i < m.length; i++) {
            max[i] = m[i][0];

            for (int j = 0; j < m[i].length; j++) {
                if (max[i] < m[i][j]) {
                    max[i] = m[i][j];
                }
            }
        }
        return max;
    }

    //2. variant
    private static int[] reaMaxid2(int[][] m){
        int[] results = new int[m.length];

        for (int i = 0; i < m.length; i++){
            int curMax = Integer.MIN_VALUE;
            for (int j = 0; j < m[i].length; j++){
                if (m[i][j] > curMax){
                    curMax = m[i][j];
                }
            }
            results[i] = curMax;
        }
        return results;
    }
}
