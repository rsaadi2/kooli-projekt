package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 * Sportlase esinemist hindab n>2 kohtunikku. Hinnete hulgast eemaldatakse kõige madalam ja
 * kõige kõrgem ning leitakse ülejäänud n-2 hinde aritmeetiline keskmine.
 Kirjutada Java-meetod hinde arvutamiseks.
 Parameetriks olevat massiivi muuta ei tohi.

 public static double result (double[] marks)
 */
public class SportlaseHinded {
    //public class Answer {

        public static void main (String[] args) {
            System.out.println (result (new double[]{0., 1., 2., 3., 4.}));
            // YOUR TESTS HERE
        }

    /*
    public static void main(String[] args) {
        System.out.println(result(new double[] { 0.0, 6.0, 3.0, 9.0, 1.0, 4.0 }));

    }*/

        public static double result(double[] marks) {

        double min = Double.MAX_VALUE;
        double max = Double.MIN_VALUE;
        double sum = 0;
        double keskmine = 0;

        for (int i = 0; i < marks.length; i++) {
            sum = sum + marks[i];
        }

        for (int i = 0; i < marks.length - 1; i++) {
            if (i == 0) {
                min = Math.min(marks[i], marks[i + 1]);
                max = Math.max(marks[i], marks[i + 1]);
            } else {
                min = Math.min(min, marks[i + 1]);
                max = Math.max(max, marks[i + 1]);
            }
        }

        sum = sum - min - max;

        keskmine = sum / (marks.length - 2);

        return keskmine;
    }

}
