package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class MassiiviNegElemntideArv {
    public static void main(String[] args) {
        System.out.println ("\n == negElArv == \n");
        System.out.println (negElArv (new int[]{0}));
        System.out.println (negElArv (new int[]{0, -5, -7, 6, 9, -11}));
    }

    public static int negElArv (int[] m) { //loeb kÃµik negatiivsed arvud kokku
        int k = 0;
        for(int arv : m) if(arv < 0) k++;
        return k;
    }
}
