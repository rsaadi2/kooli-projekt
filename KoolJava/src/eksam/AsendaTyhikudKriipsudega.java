package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class AsendaTyhikudKriipsudega {
    public static void main(String[] args) {
        System.out.println("\n == asenda == \n");
        String s = "Tere, TUDddENG,   tore ARVUTI sul!";
        String t = asenda(s); // "Tere,-TUDENG,---tore-ARVUTI-sul!"
        System.out.println(s + " > " + t);
    }

    public static String asenda(String s) { //asendab kÃµik tÃ¼hikud "-" mÃ¤rgiga
        return s.replaceAll(" ", "-");
    }
}

