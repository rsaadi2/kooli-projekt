package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class ReaMiinimumideMassiiv {
    public static void main(String[] args) {
        System.out.println ("\n == reaMinid == \n");
        int[] res = reaMinid (new int[][] { {1,2,3}, {4,5,6} }); // {1, 4}
        System.out.println ("{"+res[0]+","+res[1]+"}"); // {1, 4}
    }

    public static int[] reaMinid (int[][] m) { //Loeb ja vÃ¤ljastab iga massiivi kÃµige madalama numbri
        int[] min = new int[m.length];
        for (int l = 0; l < m.length; l++) {
            min[l] = m[l][0];
            for (int i = 0; i < m[l].length; i++) {
                if (m[l][i] < min[l]) {
                    min[l] = m[l][i];
                }
            }
        }
        return min;
    }
}
