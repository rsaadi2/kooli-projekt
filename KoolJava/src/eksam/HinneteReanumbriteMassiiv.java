package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class HinneteReanumbriteMassiiv {
//public class Answer {
    /*
     * On antud hinnete maatriks (int[][] g), milles on iga üliõpilase jaoks üks
	 * rida, mille elementideks on selle üliõpilase hinded (skaalal 0 kuni 5).
	 * Koostada Java meetod üliõpilaste pingerea moodustamiseks, mis tagastaks
	 * reanumbrite massiivi (kõrgeima keskhindega reast allapoole, võrdsete
	 * korral jääb ettepoole see rida, mille number on väiksem).
	 	 */

    public static void main(String[] args) {
        int[][] grades = new int[][]{{5, 3, 1}, {4, 3, 5}};

        int[] res = sortByAvg(grades); // {1,0}
        for (int i = 0; i < res.length; i++) {
            System.out.print(res[i] + " ");
        }
    } // main

    public static int[] sortByAvg(int[][] g) {
        //Defineerime paar muutujat mida vaja on alguses
        int[] pingerida = new int[g.length];
        double[] keskmised = new double[g.length];
        double keskmine = 0;

        //Käime kõik õpilased läbi
        for (int i = 0; i < g.length; i++) {
            //Arvutame õpilase keskmise hinde
            keskmine = 0;
            for (double hinne : g[i]) keskmine += hinne;
            keskmine = keskmine / g[i].length;
            //Paneme keskmise hinde oma massiivi
            keskmised[i] = keskmine;
        }

        //Nüüd käime kõik õpilased uuesti läbi võrdluse jaoks
        for (int j = 0; j < g.length; j++) {
            //õpilase järjekorra number
            int suuremad = 0;
            //käime kõik keskmised läbi
            for (int x = 0; x < keskmised.length; x++) {
                //Kui kellegil on paremad hinded kui käes oleval õpilasel, siis käesoleva õpilase järjekord suureneb ühe võrra
                if (keskmised[x] > keskmised[j]) suuremad++;
                else if (keskmised[x] == keskmised[j]) {
                    //kui hinded on samad, siis vaadatakse kumb ennem nimekirjas oli
                    if (j > x) suuremad++;
                }
            }
            //sisestame õpilase meie tehtud järjekorda ja massiivi väärtuseks tema algne järjekorra koht
            pingerida[suuremad] = j;
        }
        return pingerida;

    }
}
