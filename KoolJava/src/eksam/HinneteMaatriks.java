package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class HinneteMaatriks {
    //on antud hinnete maatriks, milles on iga yliõpilase jaoks yks rida, mille elementideks on selle
//yliõpilase hinded skaalal 0-5. koosta java meetod yliõpilaste pingerea moodustamiseks, mis tagastaks
//reanumbrite massiivi (kõrgeima keskhindega reast allapoole, võrdsete korral jääb ettepoole see rida,
//mille number on väiksem)

    public static void main(String[] args) {

        int[] res = reaMaxid(new int[][]{{1, 2, 3},
                {4, 5, 6}});

        System.out.println(Integer.toString(res[0]));
        System.out.println(Integer.toString(res[1]));
    }

    public static int[] reaMaxid(int[][] m) {
        int[] max = new int[m.length];
        for (int l = 0; l < m.length; l++) {
            max[l] = m[l][0];
            for (int i = 0; i < m[l].length; i++) {
                if (m[l][i] > max[l]) {
                    max[l] = m[l][i];
                }
            }
        }
        return max;
    }
}

