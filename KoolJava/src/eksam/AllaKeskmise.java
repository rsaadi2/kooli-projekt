package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java meetod, mis leiab etteantud reaalarvude massiivi d põhjal niisuguste elementide arvu, mis on rangelt väiksemad kõigi elementide aritmeetilisest keskmisest (aritmeetiline keskmine = summa / elementide_arv).
 Write a method in Java to find the number of elements strictly less than arithmetic mean of all elements of a given array of real numbers d (arithmetic mean = sum_of_elements / number_of_elements).
 public static int allaKeskmise (double[] d)
 */
public class AllaKeskmise {
    //public class Answer {

    public static void main(String[] args) {
       // System.out.println ("\n == allaKeskmise == \n");
        System.out.println (allaKeskmise (new double[]{0.}));
        System.out.println (allaKeskmise (new double[]{5., -7., -10., 5., 9.}));

    }

    public static int allaKeskmise (double[] d) { //loeb kokku kÃµik alla keskmise olevaid arve
        double keskmine = 0.0;
        for (double arv : d) keskmine += arv;
        keskmine = keskmine / d.length;

        int k = 0;
        for (double arv : d) if (arv < keskmine) k++;

        return k;
    }
}
