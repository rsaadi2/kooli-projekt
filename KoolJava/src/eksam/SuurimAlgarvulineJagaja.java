package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 */
public class SuurimAlgarvulineJagaja {
    //public class Answer {

    /*  On antud positiivne täisarv n. Kirjutada Java meetod, mis leiab n suurima algarvulise jagaja.
        Koodis 2. varianti, peab katsetama
	*/

    public static void main(String[] args) {
        System.out.println(greatestPrimeFactor(1234)); // 617
        System.out.println(greatestPrimeFactor(2)); // 2
        System.out.println(greatestPrimeFactor(3)); // 3
        System.out.println(greatestPrimeFactor(1)); // 1
        System.out.println(greatestPrimeFactor(120)); // 5
        System.out.println("2. variant " + maxPrimeNumber(120));
    }

       public static int greatestPrimeFactor(int n) {
    	/*
		 * HOIATUS: mul pani timeouti, arvatavasti lähenemine vale
		 * Muidu töötab..
		 */

        int prime = 0;
        // hakkame kõiki arve läbi käima alates kõige suuremast ehk "n"ist
        for (int i = n; i >= 1; i--) {
            //vaatame kas käes olev arv jagub ideaalselt n-iga
            if (n % i == 0) {
                //eeldame alguses et on tegemist algarvuga
                boolean algarv = true;
                //nüüd vaatame kõik arvud algusest läbi (alates 2-st sest 1ga jaguvad kõik)
                for (int j = 2; j <= i; j++) {
                    //kui arv jagub mingi muu arvuga ja see ei ole arv ise
                    if (i % j == 0 && j != i) {
                        //märgmime et leitsime mingi muu vaste
                        algarv = false;
                        //lõpetame kontrollimise
                        break;
                    }
                }
                //kui kontroll läbitud ja eeldus on ikka paigas, siis see peaks olema kõige kõrgem algarv
                if (algarv == true) {
                    prime = i;
                    //lõpetame otsimise
                    break;
                }
            }
        }
        //tagastame algarvu mis tuli
        return prime;
    }

    // 2. variant
    public static long maxPrimeNumber(long input){
        long curVal = input;
        long curDivider = 2;
        while (true){
            if (curVal == curDivider){
                return curDivider;
            }else if(curVal % curDivider == 0){
                curVal = curVal / curDivider;
            }else{
                curDivider++;
            }
        }
    }

}

