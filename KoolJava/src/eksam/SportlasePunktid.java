package eksam;

/**
 * Created by Kasutaja on 27.01.2016.
 * Sportlase punktisumma arvutatakse üksikkatsetest saadud punktide summana,
 * millest on maha võetud kahe halvima katse tulemused (üksikkatseid on rohkem kui kaks).
 * Kirjutada Java meetod, mis arvutab punktisumma üksikkatsete tulemuste massiivi põhjal.
 * Parameetriks olevat massiivi muuta ei tohi.
 * public static int score (int[] points)
 */
public class SportlasePunktid {
    //public class Answer {
    public static void main(String[] args) {
        System.out.println(score(new int[]{4, 1, 2, 3, 0})); // 9
        System.out.println(score(new int[]{-2, -1, -1, 0})); // -1

    }
    public static int score (int[] points){
        int vaikseim = points[0];
        int minPos1 = 0;
        for(int a = 0; a < points.length; a++){
            if(points[a] < vaikseim){
                vaikseim = points[a];
                minPos1 = a;
            }
        }

        int vaike = points[1];
        for(int b = 0; b < points.length; b++){
            if(points[b] < vaike && points[b] >= vaikseim){
                if(b != minPos1){
                    vaike = points[b];
                }
            }
        }

        int skoor = 0;
        for(int c: points){
            skoor += c;
        }
        skoor = skoor-vaike-vaikseim;
        return skoor;
    }
}