package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 * tee õpilaste pingerida
 */
public class HinneteSortByAvg {
    public static void main(String[] args) {
        System.out.println ("\n == sortByAvg == \n");
        int[] rese = sortByAvg (new int[][] { {4, 0, 0}, {1, 2, 0}, {4, 0, 0} }); //  {0, 2, 1}
        //{5,3,1},{4,3,5}}
        //{{4, 0, 0}, {1, 2, 0}, {4, 0, 0}}
        //{{1, 2, 3}, {4, 5}, {2}}
        for (int i=0; i < rese.length; i++) {
            System.out.print (rese[i] + " ");
        }
    }

    public static int[] sortByAvg (int[][] g) {
        //Defineerime paar muutujat mida vaja on alguses
        int[] pingerida = new int[g.length];
        double[] keskmised = new double[g.length];
        double keskmine = 0;

        //Käime kõik õpilased läbi
        for (int i = 0; i < g.length; i++) {
            //Arvutame õpilase keskmise hinde
            keskmine = 0;
            for (double hinne : g[i]) keskmine += hinne;
            keskmine = keskmine / g[i].length;
            //Paneme keskmise hinde oma massiivi
            keskmised[i] = keskmine;
        }

        //Nüüd käime kõik õpilased uuesti läbi võrdluse jaoks
        for (int j = 0; j < g.length; j++){
            //õpilase järjekorra number
            int suuremad = 0;
            //käime kõik keskmised läbi
            for (int x = 0; x < keskmised.length; x++){
                //Kui kellegil on paremad hinded kui käes oleval õpilasel, siis käesoleva õpilase järjekord suureneb ühe võrra
                if (keskmised[x] > keskmised[j]) suuremad++;
                else if(keskmised[x] == keskmised[j]){
                    //kui hinded on samad, siis vaadatakse kumb ennem nimekirjas oli
                    if(j > x) suuremad++;
                }
            }
            //sisestame õpilase meie tehtud järjekorda ja massiivi väärtuseks tema algne järjekorra koht
            pingerida[suuremad] = j;
        }
        return pingerida;
    }

}
