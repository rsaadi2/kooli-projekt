package eksam;

/**
 * Created by Kasutaja on 26.01.2016.
 * Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda n täisarvumaatriksi,
 * mille iga elemendi väärtuseks on minimaalne selle elemendi reaindeksist ja veeruindeksist
 * (indeksid algavad nullist).
 public static int[][] muster (int n)
 */
public class MinReaJaVeeruIndeksist {
    public static void main (String[] args) {
        muster (5);
    }

    public static int[][] muster (int n) {
        int[][] muster = new int[n][n];

        for (int i = 0; i < n; i++) {

            for (int j = 0; j < n; j++) {

                if (i < j){

                    muster[i][j] = i;
                    System.out.print(i);
                }
                else if (i >= j){

                    muster[i][j] = j;
                    System.out.print(j);
                }
            }
            System.out.println();

        }
        return muster;
    }
}
