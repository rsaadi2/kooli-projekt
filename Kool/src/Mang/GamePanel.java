package Mang;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer; //updatemiseks vajalik! See võimaldab teha pausi update ajal. 



public class GamePanel extends JPanel implements ActionListener, KeyListener {
//peame tegema GamePaneli Jframe koostisosaks(JPanel), et me saaks JFrame vűimalusi selles kasutada. 
	//Ja nŁŁd oli meil olemas window!
	Player player = new Player (); 
Ball ball = new Ball(55, 111);
Ball ball1 = new Ball(25, 333);
Ball ball2= new Ball(51, 45);
Ball ball3 = new Ball(35, 25);
Ball ball4 = new Ball(75, 456);

Sein sein = new Sein ( 100, 0 );
Sein sein1 = new Sein ( 200, 50 );
Sein sein2= new Sein ( 300, 70 );
Sein sein3 = new Sein ( 400, 20 );
// 

	//List<Ball> rects = new ArrayList<Ball>();


	
	
	
	// enum on nagu tavaline tyyp, aga tema eripšrane on selles, et sa annad talle vššrtused ette, mida 
	//ta saab omistada
	
	public static  enum STATE {
		MENU,
		GAME
	};
	public static STATE State = STATE.MENU;
	public Menu menu;
		
	
	
	public GamePanel() {
		//konstruktor! hetkel ei tee sellega midagi muud!
		
		Timer tim = new Timer(15, this);//oluline! Esimene argument Łtleb, kui pik on paus. Teine argument -
		//mis ta siis teeb, kui paus tekib? vajab action listener klassi! Seega laiendame seda sama klassi
		//trŁkkides Łles "implements ActionListener. Seega ongi teine argument this, kuna see sama klass
		//ongi action listener klass!
		tim.start();
		
		this.addKeyListener(this);// Jpanel meetod. This argumendina Łtlebki, et see praegune klass on Listener!
		this.setFocusable(true); // no kui pole aken aktiivne, siis klaviatuur ka mitte. loogish. 
		  menu = new Menu ();
		this.addMouseListener(new MouseInput());
	}
	
	//Siia tuleb 2d standardmšngu esimene osa, update meetod. Selle loome ise, me ei kasuta mingit 
	//Jpaneli meetodit. 
	private void update(){//tegi privaatseks, kuna mitte Łksi teine klas ei vaja sellele ligipššsu, "miks sisi
//		ka mitte teha privaatseks" teglt műttetu seda kirjutada. 
		if(State == STATE.GAME){
			System.out.println(ball.x);
			
				
		
		player.update();
			
			ball.update();
			ball1.update();
			ball2.update();
			ball3.update();
			ball4.update();
			
			sein.update();
			sein1.update();
			sein2.update();
			sein3.update();	
	
		
			
			
		//hšsti loogiline, updateme palli ja mšngijat, nŁŁd kuna soovime luua kokupűrget, siis seejšrel tulebki
		//updateda, kas kokkuűrge on toimunud. 
		ball.checkCollusionWith(player);
		ball1.checkCollusionWith(player);
		ball2.checkCollusionWith(player);
		ball3.checkCollusionWith(player);
		ball4.checkCollusionWith(player);
		sein.checkCollusionWith(player);
		sein1.checkCollusionWith(player);
		sein2.checkCollusionWith(player);
		sein3.checkCollusionWith(player);
		//ehk siis kutsume všlja ball klassist meetodi ja anname selle 
		//edasi player klassi. 
		}else if (State == STATE.MENU){
			
		}
	}
	
	
	//2d mšng koosneb sisuliselt loopist: update, paint, clear. nagu paberi animatsioon. 
	// NŁŁd kasutamegi Jpaneli meetodit, et luua paint osa mšngule. 
	public void paintComponent(Graphics g){
		
		//Siia me paneme kogu oma koodi, mis joonistab/všrvib mšngu!
		
		g.setColor(Color.BLACK); //nŁŁd iga kord, ennem, kui pall saab všrvitud, všrvin see ekraani valgeks
		g.fillRect(0, 0, Pong2.window_width, Pong2.window_height);
		if(State == STATE.GAME){
			
				
		player.paint(g);//laseme player klassis tal endal end všrvida ja anname selle edasi G parameetrina!
		
			
			ball.paint(g);
			ball1.paint(g);
			ball2.paint(g);
			ball3.paint(g);
			ball4.paint(g);
			sein.paint(g);
			sein1.paint(g);
			sein2.paint(g);
			sein3.paint(g);
		}else if(State == STATE.MENU){
			menu.paint(g);
		}
		
	}
	public void actionPerformed(ActionEvent e){ //see on meetod, mida actionlistener vajab!!!
		//See on meie gameloop!!!!
		update();//kutsub Łleval oleva private update meetodi
		repaint(); //Jpanel meetod, kutsub iga kord pšrast update paint componendi všlja
	}
	
	public void keyPressed(KeyEvent e){
		if(State == STATE.GAME){
			
		if(e.getKeyCode() == KeyEvent.VK_UP){
			player.setYVelocity(-13); // ehk siis kui klaviatuuril vajutatakse up noolt, kutsutakse všlja
			//meetod, mille lűime plater klassis. aga enne seda lűime siia klassi objekti vűi konstruktori
			// player. Selle kohta kŁsi martinilt. 
		}
			else if(e.getKeyCode() == KeyEvent.VK_DOWN){
				player.setYVelocity(13);
			}
		if(e.getKeyCode() == KeyEvent.VK_LEFT){
			player.setXVelocity(-9); // ehk siis kui klaviatuuril vajutatakse up noolt, kutsutakse všlja
			//meetod, mille lűime plater klassis. aga enne seda lűime siia klassi objekti vűi konstruktori
			// player. Selle kohta kŁsi martinilt. 
		}
			else if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				player.setXVelocity(9);
			}
		}
		}
	
	public void keyReleased(KeyEvent e)	{
		if(State == STATE.GAME){
		if(e.getKeyCode() == KeyEvent.VK_UP ){
			player.setYVelocity(0);
		}
		else if (e.getKeyCode()== KeyEvent.VK_DOWN){
			player.setYVelocity(0);
		}
		if(e.getKeyCode() == KeyEvent.VK_LEFT ){
			player.setXVelocity(0);
		}
		else if (e.getKeyCode()== KeyEvent.VK_RIGHT){
			player.setXVelocity(0);
		}
		
		}
		
	}
	public void keyTyped(KeyEvent e){//see siuke veidi všhem kasutatud meetod sellest klassist, kuna tŲŲtab
		//ainult siis, kui vajutada ja kohe lahti lasta. aga pole alati všga usaldatav. 
	
	}
	}
