package Mang;

import java.awt.Color;
import java.awt.Graphics;

public class Player {

	int y = Pong2.window_height / 2;// tegin ikka x juurde, kui tahan pedaali asukohta muuta, 
	//on lihtsam aint seda nr muuta
	int x = Pong2.window_width - 30;
	int yVelocity = 0;  // mšrkiisn selle 0, kuna me ju ei soovi, et meie pedaal kohe liiguks. 
	//mšngija puhul peame ju liikuma ainult Łles ja alla, seega y piisab
	int xVelocity = 0;
	public int width = 40;
	public int height = 40;
	
	public int elud  = 3;
	
	public Player(){// konstruktor!
		
	}
	public void update(){ //nagu eelmiste objektide puhulgi pean selllega ka update meetodi esile kutsuma
		// siia kirjutame mida tahame teha player objektiga. Me tegime Game panelile veel Łhe implementationi
		// keylisteneri, andios kohge veateate, kuna nagu ka actionlistener nűuab see kindlaid meetodeid,
		//mida me panime taaskord gamepanel klassi kirja. ja kui vajutatakse űiget nuppu(mille oleme 
		//defineerinud gamepanel klassis, siis kutsutakse všlja meetod, mille paneme siia kirja)
		
		
		
		if( y < 0)
		{y = 0;}
		y = y + yVelocity;
		x = x + xVelocity;
	}
	
	public void paint(Graphics g){
		g.setColor(Color.PINK);	
		g.fillRect(x, y,width, height);}
	
	public void setYVelocity(int speed){
		yVelocity = speed;
	}
	public void setXVelocity(int speed){
		xVelocity = speed;
	}
	//teeme kaks hšsti lihtsat meetodit, mis všljastavad player x ja y kooordinaadi. ja kutsume ball klassisty
	//neid meetode všlja, et saada teada playeri asukoht. 
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
}

